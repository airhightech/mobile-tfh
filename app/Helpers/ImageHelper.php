<?php

namespace App\Helpers;

use Image;

class ImageHelper
{
    public $id;

    protected $data;
    
    protected $filesize;

    public function __construct($data)
    {
        $this->data = $data;
    }
    
    public function getRelativeUrl()
    {
        return '/image/'.$this->data->filepath;
    }
    
    public function getRelativeThumUrl($dimension = '100x100')
    {
        return '/thumb/'.$dimension.'/'.$this->data->filepath;
    }

    public function getUrl()
    {
        return url('/image/'.$this->data->filepath);
    }

    public function getThumbUrl($dimension = '100x100', $create = false)
    {
        if ($create) {
            $this->createThumb($dimension);
        }

        return url('/thumb/'.$dimension.'/'.$this->data->filepath);
    }
    
    public function filesize()
    {
        return $this->filesize;
    }

    public function createThumb($dimension)
    {
        return self::createThumbFromFile($dimension, $this->data->filepath);
    }

    public static function createThumbFromFile($dimension, $file)
    {
        $filename = 'thumb-'.$dimension.'-'.$file;
        $filepath = storage_path('app/public/'.$filename);

        $img = null;

        if (!file_exists($filepath)) {
            $original = storage_path('app/public/'.$file);

            if (file_exists($original) && is_readable($original)) {
                $img = Image::make($original);
                list($width, $height) = explode('x', $dimension);
                $img->fit($width, $height);
                $img->save($filepath);
                
               // $this->filesize = filesize($filepath);
            }

        } elseif (is_readable($filepath)) {
            $img = Image::make($filepath);
            //$this->filesize = filesize($filepath);
        }

        return $img;
    }
    
    public static function resize($file) {
        $img = Image::make($file);
        $img->resize(960, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $img->save();
    }

    public static function resizeAndAddWatermark($file)
    {
        $img = Image::make($file);
        $img->resize(960, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $img->save();

        /*$img = Image::make($file);
        $width = $img->width();
        $height = $img->height();

        $img->text('ThaiFullHouse (c) ' . date('Y'), $width / 2, $height / 2, function ($font) {
            $font_file = public_path().'/fonts/Signika-Bold.ttf';
            $font->file($font_file);
            $font->size(50);
            $font->color(array(255, 255, 255, 0.5));
            $font->align('center');
            $font->valign('middle');
        });

        $img->save();*/
        
        //$this->filesize = filesize($file);
    }
    
    public static function remove($file) {
        
        $path = storage_path('app/public/*'.$file);
        
        foreach(glob($path) as $filepath) {
            @unlink($filepath);
        }    
        
        
    }
}
