<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class UploadHelper {

    protected $request;
    protected $storage = 'app/public';
    protected $errors;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function saveSingle($name) {

        $path = '';

        $this->errors = [];

        if ($this->request->hasFile($name)) {
            $file = $this->request->file($name);

            $extension = $file->getClientOriginalExtension();
            $filename = $this->getUniqueFileName() . '.' . strtolower($extension);

            try {
                $file->move(storage_path($this->storage), $filename);
                $path = $filename;
            } catch (FileException $ex) {
                $this->errors[] = $ex->getMessage();
            }
        }

        return $path;
    }

    public function saveMultiple($name) {

        $names = [];
        $this->errors = [];

        if ($this->request->hasFile($name)) {
            $files = $this->request->file($name);

            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $filename = $this->getUniqueFileName() . '.' . strtolower($extension);

                try {
                    $file->move(storage_path($this->storage), $filename);
                    $names[] = $filename;
                } catch (FileException $ex) {
                    $this->errors[] = $ex->getMessage();
                }
            }
        }

        return $names;
    }

    public function getErrors() {
        return $this->errors;
    }

    protected function getUniqueFileName() {
        return uniqid() . '-' . strtolower(base_convert(time(), 10, 16));
    }

}
