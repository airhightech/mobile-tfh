<?php
if (!function_exists('__asset')) {

    function __asset($file) {
        return $file . '?t=' . filemtime(public_path() . $file);
    }

}

if (!function_exists('__dbconf')) {

    function __dbconf($token, $translate = false) {

        $conf = \DB::table('settings')
                ->where('domain', $token)
                ->value('value');

        if ($translate) {
            $locale = \App::getLocale();
            
            $translations = json_decode($conf, true);
            
            $conf = trim(array_get($translations, $locale));
            
            if (empty($conf)) {
                $conf = trim(array_get($translations, 'th'));
            }
        }

        return $conf;
    }
}


if (!function_exists('__trget')) {

    function __trget($data, $lang = null, $default_lang = 'th') {
        
        if ($lang == null) {
            $lang = \App::getLocale();
        }
        
        $tr = trim(array_get($data, $lang));
        return $tr ? $tr : array_get($data, $default_lang);
    }

}

if (!function_exists('__knum')) {

    function __knum($value) {

        $result = 0;

        if ($value > 999 && $value <= 999999) {
            $result = floor($value / 1000) . 'K';
        } elseif ($value > 999999) {
            $result = floor($value / 1000000) . 'M';
        } else {
            $result = $value;
        }

        return $result;
    }

}

if (!function_exists('__t')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function __t($token, $default, $description = '') {

        if (preg_match('/(\w+)(\.)(.+)/', $token, $matches)) {

            $locale = \App::getLocale();

            $translation = trans('_generated_' . $token);

            if ($translation != '_generated_' . $token) {
                return empty($translation) ? $default : $translation;
            }

            $text = \DB::table('translations')
                    ->where('section', $matches[1])
                    ->where('label', $matches[3])
                    ->value('translation');

            if (empty($text)) {

                $langs = array_keys(\App\Helpers\Language::getActives());

                $now = date('Y-m-d H:i:s');

                $description = empty($description) ? $default : $description;

                DB::table('translations')
                        ->insert([
                            'section' => $matches[1],
                            'label' => $matches[3],
                            'translation' => json_encode(array_fill_keys($langs, '')),
                            'description' => $description,
                            'created_at' => $now,
                            'updated_at' => $now
                ]);

                return empty($default) ? $translation : $default;
            } else {

                return $default ? $default : trans($token);
            }
        } else {
            return $default ? $default : trans($token);
        }
    }

}

if (!function_exists('sortable_col')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function sortable_col($text, $link, $col, $default, $current) {

        if ($current[0] == $col) {
            ?>
            <a href="<?php echo $link; ?>?sortby=<?php echo $current[0]; ?>,<?php echo $current[1] == 'asc' ? 'desc' : 'asc'; ?>"
               class="sortable">
                   <?php
                   echo $text;
                   if ($current[1] == 'asc') {
                       ?>
                    <i class="fa fa-angle-up"></i>
                    <?php
                } else {
                    ?>
                    <i class="fa fa-angle-down"></i>
                    <?php
                }
                ?>
            </a>            
            <?php
        } else {
            ?>
            <a href="<?php echo $link; ?>?sortby=<?php echo $col; ?>,<?php echo $default; ?>">
                <?php echo $text; ?>
            </a>
            <?php
        }
    }

}

