<?php

namespace App\Helpers;

class Language {

    public static function getActives() {
        return [
            'en' => 'English',   
            'th' => 'ภาษาไทย',                     
            'ja' => '日本語',
            'zh' => '中文(简体)',
            'ko' => '한국어'
        ];
    }
    
    public static function getActiveCodes() {
        return array_keys(self::getActives());
    }

}
