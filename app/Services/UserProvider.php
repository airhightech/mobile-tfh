<?php

namespace App\Services;

use Auth;
use App\Models\User;
use App\Models\User\Type as UserType;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserProvider {

    public static function findOrCreateFromFB($fbuser, $validated_email = false) {
        $user = null;

        try {

            $user = User::where('email', $fbuser->email)->firstOrFail();
            $user->realname = $fbuser->name;
            $user->save();
            
        } catch (ModelNotFoundException $ex) {

            $ex = null;

            $type = UserType::where('code', '=', 'visitor')->first();

            $user = new User;
            $user->email = $fbuser->email;
            $user->type_id = $type->id;
            $user->realname = $fbuser->name;
            $user->email_validated = $validated_email;

            if (!$validated_email) {
                $user->email_validation_token = str_random(20);
            }

            $user->save();
        }

        return $user;
    }

}
