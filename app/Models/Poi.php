<?php

namespace App\Models;

use DB;

/**
 * Condominium class
 */
class Poi extends BaseModel {

    protected $table = 'search_pois';

    public function getLatLng() {
        $latlng = DB::select('SELECT ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng,
                        ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat FROM search_pois WHERE id = ?', [$this->id]);
        return $latlng[0];
    }
    
    public function toJson($options = 0) {
        
        $rows = DB::select('SELECT ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng,
                        ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat FROM search_pois WHERE id = ?', [$this->id]);
        
        $latlng = $rows[0];
        
        $type = $this->type ? $this->type : 'bts';
        
        return json_encode([
            'id' => $this->id,
            'name' => $this->getJsonTranslatedField('name'),
            'lat' => $latlng->lat,
            'lng' => $latlng->lng,
            'zl' => $this->zoom_level,
            'type' => $this->type
        ]);
    }

}
