<?php

namespace App\Models\Geo;

use DB;
use App\Models\BaseModel;

/**
 * This class define user
 */
class Province extends BaseModel {

    /**
     * This class is associated with `condo_list` table
     */
    protected $table = 'provinces';
    
    public function getDistrictCount()
    {
        return DB::table('districts')->where('province_id', $this->id)->count();
    }
}