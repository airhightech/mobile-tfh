<?php

namespace App\Models\Geo;

use App\Models\BaseModel;
use DB;

/**
 * This class define user
 */
class District extends BaseModel {

    /**
     * This class is associated with `condo_list` table
     */
    protected $table = 'districts';
    
    public function getAreaCount()
    {
        return DB::table('geo_area')->where('district_id', $this->id)->count();
    }
}