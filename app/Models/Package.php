<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Auth;
use DB;

class Package extends Model {

    const TYPE_EXCLUSIVE = 'exclusive';
    const TYPE_FEATURED = 'featured';

    protected $table = 'packages';

    public function createPurchase(Request $request) {

        if (Auth::check()) {
            
            $user = Auth::user();
            $account = $user->getPurchaseAccount();
            
            $invoice_id = $this->getInvoiceId($user);
            
            $current_price = $this->discounted_price > 0 ? $this->discounted_price : $this->normal_price;

            $purchase = new Purchase;
            $purchase->account_id = $account->id;
            $purchase->package_id = $this->id;
            $purchase->package_name = $this->name;
            $purchase->price = $current_price;
            $purchase->invoice_id = $invoice_id;
            $purchase->listing_count = $this->listing_count;
            $purchase->free_listing_count = $this->free_listing_count;
            $purchase->period = $this->period;
            $purchase->payment_method = $request->input('poption');
            $purchase->bank = $request->input('poption');
            $purchase->total_amount = $current_price * 107;
            $purchase->save();
            
            return $purchase;
            
        }
        
        return null;
    }
    
    public function getInvoiceId($user) {
        
        $count = 1;
        $invoice_id = 0;
        
        while ($count > 0) {
            $time = strtoupper(base_convert(time(), 10, 36));
            $time = substr($time, 0, 6);
            $invoice_id = $time . strtoupper(str_random(7));
            $count = DB::table('customer_purchases')->where('invoice_id', $invoice_id)->count();
        }
        
        return $invoice_id;
        
    }

}
