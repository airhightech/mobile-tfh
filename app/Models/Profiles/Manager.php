<?php

namespace App\Models\Profiles;

use App\Models\User;

/**
 * Website manager class handler
 */
class Manager extends User
{
    /**
     * Admin account
     */
    const ADMIM         = 'admin';

    /**
     * Editor account
     */
    const EDITOR        = 'editor';

    /**
     * Accountant account
     */
    const ACCOUNTING    = 'accounting';
}
