<?php

namespace App\Models\Profiles;

use App\Models\User;

/**
 * Customer class handler
 */
class Member extends User
{
    /**
     * Private owner profile
     */
    const OWNER             = 101;

    /**
     * Freelance agent profile
     */
    const FREELANCE_AGENT   = 102;

    /**
     * Company agent profile
     */
    const COMPANY_AGENT     = 103;

    /**
     * Land developer profile
     */
    const LAND_DEVELOPER    = 104;
}
