<?php

namespace App\Models;

use DB;
use App\Helpers\ImageHelper;
use App\Models\User\Profile;
use App\Models\Properties\Details as PropertyDetails;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Project\PropertyReview;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;

//use App\Models\Poi;

/**
 * Property class
 */
class Property extends BaseModel {

    use SoftDeletes;

    const LISTING_RENT = 'rent';
    const LISTING_SALE = 'sale';
    const TYPE_CONDO = 'condo';
    const TYPE_APPARTMENT = 'appartment';
    const TYPE_TOWNHOUSE = 'townhouse';
    const TYPE_DETACHED_HOUSE = 'detachedhouse';

    protected $table = 'properties';
    protected $appends = ['images', 'details'];
    protected $hidden = ['poster_id', 'province_id', 'discrict_id', 'area_id', 'location', 'created_at', 'updated_at', 'published',
        'publication_date', 'expiration_date', 'deleted_at'];

    public function getImages() {
        $images = [];

        $media = DB::select('SELECT * FROM properties_have_media WHERE property_id = ? AND media_type = ?', [$this->id, 'image']);

        if (count($media)) {
            foreach ($media as $row) {
                $images[] = new ImageHelper($row);
            }
        }

        return $images;
    }

    public function countImages() {
        return DB::table('properties_have_media')
                        ->where('property_id', $this->id)
                        ->where('media_type', 'image')
                        ->count();
    }

    public function getLatLng() {
        $location = DB::select('SELECT ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng, ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat FROM properties WHERE id = ?', [
                    $this->id
        ]);

        if (count($location)) {
            return array_shift($location);
        } else {
            return false;
        }
    }

    public function getDetails() {
        $details = PropertyDetails::find($this->id);
        if ($details == null) {
            $details = new PropertyDetails;
        }
        return $details;
    }

    public function publish() {
        $this->published = true;
        $this->publication_date = date('Y-m-d');
        $this->save();
    }

    public function unpublish() {
        $this->published = false;
        $this->save();
    }

    public function getStatus() {
        $publication_time = $this->getPublicationDuration();

        if ($this->published) {
            $pubdate = strtotime($this->publication_date);
            if (($pubdate + $publication_time) < time()) {
                return 'present';
            } else {
                return 'expired';
            }
        } else {
            return 'draft';
        }
    }

    protected function getPublicationDuration() {
        return 30 * 3600;
    }

    public function getPublicationPeriode() {
        if ($this->publication_date) {
            $start = date('d/m/Y', strtotime($this->publication_date));
            $end = date('d/m/Y', strtotime($this->expiration_date));
            return $start . ' - ' . $end;
        } else {
            return 'N/A';
        }
    }

    public function getListingClass() {
        return 'Standard';
    }

    public function getListingThumb() {
        $filepath = DB::table('properties_have_media')->where('property_id', $this->id)->first();
        if ($filepath) {
            return new ImageHelper($filepath);
        } else {
            return null;
        }
    }

    public function isInUserFavorites($user) {
        $favorites = DB::select('SELECT * FROM user_favorite_properties WHERE user_id = ? AND property_id = ?', [$user->id, $this->id]);
        return count($favorites) > 0;
    }

    public function getPublisherProfile() {
        return Profile::find($this->poster_id);
    }

    public function getImagesAttribute() {
        $media = DB::select('SELECT * FROM properties_have_media WHERE property_id = ? AND media_type = ?', [$this->id, 'image']);
        $images = [];

        if (count($media)) {
            foreach ($media as $image) {
                $images[] = url('/image/' . $image->filepath);
            }
        }

        return $images;
    }

    public function getDetailsAttribute() {
        return $this->getDetails();
    }

    public function getProvince() {
        return '';
    }

    public function getProjectReview() {

        $review = null;

        try {
            $review = PropertyReview::where('property_id', $this->id)->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            $review = new PropertyReview;
            $review->property_id = $this->id;
            $review->save();
        }

        return $review;
    }

    public function getNearbyPlaces() {

        $lnglat = $this->getLatLng();

        return DB::table('search_pois')
                        ->select('*'
                                , DB::raw("ST_Distance(location, ST_GeogFromText('SRID=4326;POINT({$lnglat->lng} {$lnglat->lat})')) as distance")
                                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
                        )
                        ->whereRaw("ST_Distance(location, ST_GeogFromText('SRID=4326;POINT({$lnglat->lng} {$lnglat->lat})')) < " . 1000)
                        ->get();
    }
    
    public function getListedOn() {
        return Carbon::now()->subSeconds(strtotime($this->publication_date))->diffForHumans();  
    }

}
