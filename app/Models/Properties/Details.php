<?php

namespace App\Models\Properties;

use DB;
use App\Models\BaseModel;
use App\Models\Property;
use App\Models\Geo\Area;

class Details extends BaseModel {
    
    protected $table = 'property_details';

    protected $primaryKey = 'property_id';

    protected $hidden = ['created_at', 'updated_at'];
    
    public function getFacilityIds()
    {
        return DB::table('properties_have_facilities')->where('property_id', $this->property_id)->pluck('facility_id')->all();
    }


    public function getFeatureIds()
    {
        return DB::table('properties_have_features')->where('property_id', $this->property_id)->pluck('feature_id')->all();
    }

    public function getOptinoalFeatureIds()
    {
        return DB::table('properties_have_optional_features')->where('property_id', $this->property_id)->pluck('feature_id')->all();
    }

    public function getImages()
    {

    }

    public function getRefCode()
    {
        $property = Property::find($this->property_id);

        $listing_types = [
            'rent' => 'RT',
            'sale' => 'SL',
        ];

        $property_types = ['apartment' => 'AP', 'condo' => 'CD', 'detachedhouse' => 'DH', 'townhouse' => 'TH'];

        return array_get($property_types, $property->property_type, 'PT') . array_get($listing_types, $property->listing_type, 'UN'). sprintf('%07d', $this->property_id);
    }

    public function getListingDate()
    {
        return date('d/m/Y', strtotime($this->created_at));
    }

    public function getAvailabilityDate()
    {
        return date('d/m/Y', strtotime($this->availability_date));
    }

    public function getYoutubeVideo1()
    {
        $query = parse_url($this->ylink1, PHP_URL_QUERY);
        parse_str($query, $values);
        return array_get($values, 'v', false);
    }

    public function getYoutubeVideo2()
    {
        $query = parse_url($this->ylink2, PHP_URL_QUERY);
        parse_str($query, $values);
        return array_get($values, 'v', false);
    }

    public function getArea()
    {
        if ($this->area_id > 0) {
            $area = Area::find($this->area_id);
            return $area->getTranslatedField('name');
        }
        else {
            return '-';
        }
    }
    
    public function nearby_bts() {
        return 'BTS';
    }
    
    public function nearby_mrt() {
        return 'MRT';
    }
    
    public function nearby_apl() {
        return 'APL';
    }
    
}

