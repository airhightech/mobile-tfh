<?php

namespace App\Models\Properties;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SearchCriteria extends Model
{
    /**
     * Listing type. Acceptable values are
     * - App\Models\Property::LISTING_RENT (rent)
     * - App\Models\Property::LISTING_SALE (sale)
     */
    public $lt;

    /**
     * Property type. Acceptable values are
     * - App\Models\Property::TYPE_CONDO (co)
     * - App\Models\Property::TYPE_APPARTMENT (ap)
     * - App\Models\Property::TYPE_TOWNHOUSE (th)
     * - App\Models\Property::TYPE_DETACHED_HOUSE (dh)
     */
    public $pt;

    /**
     * Map region to filter the search result
     */
    public $region;

    /**
     *
     */
    public $price_range;

    /**
     * Create a search criteria from user request
     *
     * @return \App\Models\SearchCriteria $searchCriteria
     */
    public static function createSearchFromUserRequest(Request $request)
    {

    }

    /**
     * Generate an url that can use your
     */
    public function generateUrl()
    {

    }
}
