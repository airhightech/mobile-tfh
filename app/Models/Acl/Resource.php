<?php

namespace App\Models\Acl;

use Illuminate\Database\Eloquent\Model;

/**
 * This class define the type of user that can use the website
 */
class Resource extends Model
{
    /**
     * This class is associated with `acl_resources` table
     */
    protected $table = 'acl_resources';


    /**
     * Get the list of all provileges that can be requested for this resource
     */
    public function getAvailablePrivileges()
    {
        
    }
}