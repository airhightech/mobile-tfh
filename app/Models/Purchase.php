<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Package;
use App\Models\User\Account;
use App\Models\User;
use App\Models\Payment\Paysbuy;


class Purchase extends Model
{    
    protected $table = 'customer_purchases';
    
    public function getPackage() {
        return Package::find($this->package_id);
    }
    
    public function getAmount() {
        return ($this->updated_amount > 0 ? $this->updated_amount : $this->total_amount) / 100;
    }
    
    public function getAccount() {
        return Account::find($this->account_id);
    }
    
    public function getUser() {
        $account = Account::find($this->account_id);
        return User::find($account->creator_id); 
    }
    
    
    public function getPaybuyPayment() {
        if ($this->payment_method == 'paysbuy') {
            return Paysbuy::where('purchase_id', $this->id)->orderBy('updated_at', 'desc')->first();
        }        
        return null;
    }
}
