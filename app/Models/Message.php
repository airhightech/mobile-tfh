<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * This class handle messages sent by visitor to members
 */
class Message extends Model
{
    protected $table = 'messages';
}
