<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Language;

class BaseModel extends Model {

    /**
     * 
     * @param string $field
     * @param array $values
     */
    public function setJsonTranslatedField($field, $values) {
        $langs = array_keys(Language::getActives());
        $this->$field = array_only($values, $langs);
    }

    /**
     * 
     * @param string $field
     * @param string $lang
     * @return string
     */
    public function getJsonTranslatedField($field, $lang = null, $fallback_lang = 'th') {

        $langs = array_keys(Language::getActives());

        if ($lang == null || in_array($lang, $langs) == false) {
            $lang = \App::getLocale();
        }

        $options = json_decode($this->$field, true);

        $tr = trim(array_get($options, $lang));

        return $tr ? $tr : array_get($options, $fallback_lang);
    }

    /**
     * 
     * @param string $field
     * @param array $values
     */
    public function setTranslatedField($field, $values) {
        $this->setJsonTranslatedField($field, $values);
    }

    /**
     * @deprecated Please remove later
     * @param string $field
     * @return string
     */
    public function getTranslatedField($field) {
        return $this->getJsonTranslatedField($field);
    }

}
