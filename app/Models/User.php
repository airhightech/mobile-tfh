<?php

namespace App\Models;

use DB;

//use Illuminate\Database\QueryException;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Models\User\Profile as UserProfile;
use App\Models\User\Account as UserAccount;
use App\Models\User\FavoriteTrait;
use App\Models\User\Type as UserType;


/**
 * Main user class
 */
class User extends Authenticatable
{
    use Notifiable;
    use FavoriteTrait;   
    
    protected $hidden = [
          'email_validated'
        , 'email_validation_token'
        , 'password'
        , 'password_reset_token'
        , 'remember_token'
        , 'type_id'
        , 'created_at'
        , 'updated_at'];
    
    public function getProfile()
    {
        $profile = null;

        try {
            $profile = UserProfile::findOrFail($this->id);
        }
        catch(ModelNotFoundException $ex) {
            $profile = new UserProfile;
            $profile->user_id = $this->id;
            $profile->member_type = UserProfile::VISITOR;
            $profile->save();
        }

        return $profile;
    }
    
    public function countNewMessage()
    {
        return DB::table('messages')->where('receiver_id', $this->id)->count();
    }

    public function countProperty()
    {
        return DB::table('properties')->where('poster_id', $this->id)->count();
    }
    
    public function getTotalVisit() {
        return 0;
    }
    
    public function getPurchaseAccount() {
        
        $account_id = DB::table('customer_accounts_managers')->where('user_id', $this->id)->value('account_id');
        
        $account = null;
        
        if ($account_id > 0) {
            $account = UserAccount::find($account_id);
        }
        else {
            $account = new UserAccount;
            $account->creator_id = $this->id;
            $account->save();
            
            // Add manager
            
            DB::insert('INSERT INTO customer_accounts_managers (account_id, user_id) VALUES (?, ?)',
                    [$account->id, $this->id]);
        }
        
        return $account;
    }
    
    public function getType() {
        
        $type = null;
        
        try {
            $type = UserType::findOrFail($this->type_id);
        } catch (ModelNotFoundException $ex) {
            $type = UserType::where('code', 'visitor')->first();
            $this->type_id = $type->id;
            $this->save();
        }
        
        return $type;
        
    }
    
    public function isVisitor() {
        return $this->checkUserType('visitor');
    }
    
    public function isMember() {
        return $this->checkUserType('member');
    }
    
    public function isManager() {
        return $this->checkUserType('manager');
    }
    
    protected function checkUserType($type) {
        $usertype = UserType::where('code', $type)->first();
        return $this->type_id == $usertype->id;
    }
    
}
