<?php

namespace App\Models;

/**
 * Condominium class
 */
class Banner extends BaseModel {

    protected $table = 'banners';
    
    public static function getLocations() {
        return [
            'home' => trans('manager/banner.location-home'),
            
            'map-sale' => trans('manager/banner.location-map-sale'),
            'map-rent' => trans('manager/banner.location-map-rent'),
            'map-project' => trans('manager/banner.location-map-project'),
            'map-condo' => trans('manager/banner.location-map-condo'),
            
            'list-sale' => trans('manager/banner.location-list-sale'),
            'list-rent' => trans('manager/banner.location-list-rent'),
            'list-project' => trans('manager/banner.location-list-project'),
            'list-condo' => trans('manager/banner.location-list-condo')
        ];
    }
    
    
    public function getLocation() {
        return $this->location ? trans('manager/banner.location-' . $this->location) : '';
    }

}
