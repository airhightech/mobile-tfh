<?php

namespace App\Models\User;

use App\Models\BaseModel;
use App\Models\User;

/**
 * Condominium class
 */
class Staff extends BaseModel {
    
    protected $primaryKey = 'staff_id';
    
    protected $table = 'business_staffs';
    
    public static function getRoles() {
        return [
            'manager' => trans('manager/staff.role-manager'),
            'editor' => trans('manager/staff.role-editor'),
            'accountant' => trans('manager/staff.role-accountant'),
            'translator' => trans('manager/staff.role-translator')
        ];
    }

    public function getUser() {
        
        return User::find($this->staff_id);
    }
    
}