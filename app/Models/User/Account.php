<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

/**
 * This class define user
 */
class Account extends Model {   
    
    protected $table = 'customer_accounts';
    
    public function getCreator() {
        return User::find($this->creator_id); 
    }
}