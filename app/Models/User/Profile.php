<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\User\ProfileMutator;

/**
 * This class define user
 */
class Profile extends Model {
    
    use ProfileMutator;

    const OWNER = 'owner';
    const FREELANCE_AGENT = 'agent';
    const COMPANY_AGENT = 'company';
    const LAND_DEVELOPER = 'developer';
    const VISITOR = 'visitor';

    protected $appends = [
        'posts', 'email', 'listings', 'description',
        'comments', 'profile_picture', 'cover_picture', 'logo',
        'properties', 'name', 'langs', 'id', 'ranking', 'website', 'tel', 'price_range'
        ];

    protected $hidden = [
        'citizen_id_attachment',
        'cover_picture_path',
        'profile_picture_path',
        'accept_quick_matching',
        'created_at', 'citizen_id', 'line_id', 'languages',
        'has_paid', 'phone', 'mobile', 'firstname', 'lastname', 'company_name',
        'updated_at'
        ];

    /**
     * This class is associated with `users` table
     */
    protected $table = 'user_profiles';
    protected $primaryKey = 'user_id';

    public function getPublicID() {
        $prefix = '';

        switch ($this->member_type) {
            case self::OWNER:
                $prefix = 'O';
                break;
            case self::FREELANCE_AGENT:
                $prefix = 'A';
                break;
            case self::COMPANY_AGENT:
                $prefix = 'R';
                break;
            case self::LAND_DEVELOPER:
                $prefix = 'L';
                break;

            default:
                # code...

                $this->member_type = self::OWNER;
                $this->save();
                $prefix = 'O';

                break;
        }

        return $prefix . sprintf('%05d', $this->user_id);
    }

    public function getName() {
        $name = '';

        if ($this->member_type == self::OWNER || $this->member_type == self::FREELANCE_AGENT) {
            $name = $this->firstname . ' ' . $this->lastname;
        } else if ($this->member_type == self::COMPANY_AGENT || $this->member_type == self::LAND_DEVELOPER) {
            $name = $this->company_name;
        }

        if (empty($name)) {
            $name = $this->firstname . ' ' . $this->lastname;
        }

        if (empty($name)) {
            $name = $this->company_name;
        }

        return $name;
    }

    public function getTextAddress() {
        // $address = [$this->street_no, $this->street_no, $this->street_name, $this->area, $this->district, $this->province, $this->postal_code];
        // $address = array_filter($address);
        return $this->address;
    }

    public function getHRBirthday() {
        if ($this->birthday) {
            list($year, $month, $day) = explode('-', $this->birthday);
            return $day . '/' . $month . '/' . $year;
        } else {
            return '';
        }
    }

    public function getCoverPicture() {
        if ($this->cover_picture_path) {
            return '/thumb/1150x300/' . $this->cover_picture_path;
        } else {
            return '';
        }
    }

    public function getProfilePicture() {
        if ($this->profile_picture_path) {
            return '/thumb/300x300/' . $this->profile_picture_path;
        } else {
            return '/img/user.png';
        }
    }
    
    public function getContactPicture() {
        if ($this->profile_picture_path) {
            return '/thumb/64x64/' . $this->profile_picture_path;
        } else {
            return '/img/user.png';
        }
    }

    public function getTags() {
        return 'House, land';
    }

    public function getUser() {
        return User::find($this->user_id);
    }

    public function countBanners() {
        return 5;
    }

    public function countReview() {
        return 5;
    }

    

}
