<?php

namespace App\Models\User;

use App\Models\Property;
use DB;
use Illuminate\Database\QueryException;

trait FavoriteTrait {
    
    public function toggleFavoriteProperty($pid) {
        // Try to insert
        $result;
        try {
            DB::insert('INSERT INTO user_favorite_properties (user_id, property_id, created_at) VALUES (?, ?, NOW())', [$this->id, $pid]);
            $result = 'ADDED';
            // Vaforite created
        } catch (QueryException $ex) {
            $ex = null;
            DB::delete('DELETE FROM user_favorite_properties WHERE user_id = ? AND property_id = ?', [$this->id, $pid]);
            $result = 'REMOVED';
        }
        return $result;
    }
    
    public function toggleFavoriteAgent($aid) {
        // Try to insert
        $result;
        try {
            DB::insert('INSERT INTO user_favorite_agents (user_id, agent_id, created_at) VALUES (?, ?, NOW())', [$this->id, $aid]);
            $result = 'ADDED';
            // Vaforite created
        } catch (QueryException $ex) {
            $ex = null;
            DB::delete('DELETE FROM user_favorite_agents WHERE user_id = ? AND agent_id = ?', [$this->id, $aid]);
            $result = 'REMOVED';
        }
        return $result;
    }
    
    
    public function toggleFavoriteCondo($pid) {
        // Try to insert
        $result;
        try {
            DB::insert('INSERT INTO user_favorite_condos (user_id, condo_id, created_at) VALUES (?, ?, NOW())', [$this->id, $pid]);
            $result = 'ADDED';
            // Vaforite created
        } catch (QueryException $ex) {
            $ex = null;
            DB::delete('DELETE FROM user_favorite_condos WHERE user_id = ? AND condo_id = ?', [$this->id, $pid]);
            $result = 'REMOVED';
        }
        return $result;
    }
    
    public function getFavoriteProperties()
    {
        $ids = DB::table('user_favorite_properties')->where('user_id', $this->id)->pluck('property_id');
        return Property::whereIn('id', $ids)->get();
    }
    
    public function getFavoriteAgents()
    {
        $ids = DB::table('user_favorite_agents')->where('user_id', $this->id)->pluck('agent_id');
        return Property::whereIn('id', $ids)->get();
    }
    
    public function getFavoriteCondos()
    {
        $ids = DB::table('user_favorite_condos')->where('user_id', $this->id)->pluck('condo_id');
        return Property::whereIn('id', $ids)->get();
    }
    
}