<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * This class define the type of user that can use the website
 */
class Type extends Model
{
    /**
     * This class is associated with `user_types` table
     */
    protected $table = 'user_types';
    
    public function countUser() {
        return DB::table('users')->where('type_id', $this->id)->count();
    }
}