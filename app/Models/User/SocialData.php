<?php

namespace App\Models\User;

use DB;
use App\Models\BaseModel;
use Illuminate\Database\QueryException;

class SocialData extends BaseModel {
    
    protected $table = 'user_social_data';
    
    protected $social_network;
    
    public $user;
    
    /**
     * 
     * @param type $key
     * @param type $value
     */
    public function set($key, $value) {
        
        if ($this->user && $this->user->id > 0 && $this->social_network) {
            try {
                DB::insert('INSERT INTO user_social_data (user_id, social_network, data_key, data_value, created_at, updated_at) VALUES (?, ?, ?, ?, NOW(), NOW())', 
                        [$this->user->id, $this->social_network, $key, $value]);
            } catch (QueryException $ex) {
                $ex = null; // remove waarning for unused variable
                DB::update('UPDATE user_social_data SET data_value = ?, updated_at = NOW() WHERE user_id = ? AND social_network = ? AND data_key = ?', 
                        [$value, $this->user->id, $this->social_network, $key]);
            }
        }
        else {
            
        }        
    }   
    
    /**
     * 
     * @param type $key
     * @return type
     */
    public function get($key) {
        $value = null;
        if ($this->user && $this->user->id > 0 && $this->social_network) {
            $value = DB::table('user_social_data')
                        ->where('user_id', $this->id)
                        ->where('social_network', $this->social_network)
                        ->where('data_key', $key)
                        ->value('data_value');
        }
        return $value;
    }
    
    /**
     * 
     * @param type $key
     */
    public function delete($key) {
        if ($this->user && $this->user->id > 0 && $this->social_network) {
            DB::delete('DELETE FROM user_social_data WKERE user_id = ? AND social_network = ? AND data_key = ?',
                    [$this->user->id, $this->social_network, $key]);
        }
    }
}