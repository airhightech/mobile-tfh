<?php

namespace App\Http\Controllers;

use App\Models\Property;
use Auth;
use DB;

class UserController extends Controller {
    
    public function getPage() {
        return view('user.page'); 
    }
    
    public function getFavorites() {
        
        $favorites = [];
        
        if (Auth::check()) {
            $user = Auth::user();
            $ids = DB::table('user_favorite_properties')
                    ->where('user_id', $user->id)
                    ->orderby('created_at', 'desc')
                    ->limit(10)
                    ->pluck('property_id');
            
            $favorites = Property::whereIn('id', $ids)->get();
        }
        
        return view('user.favorites', ['favorites' => $favorites]); 
    }
    
}