<?php

namespace App\Http\Controllers;

class ProjectController extends Controller {
    
   
    public function getApartment() {
        return view('project.apartment');
    }
    
    public function getCondo() {  
        return view('project.condo'); 
    }
    
    public function getHouse() { 
        return view('project.house');
    }
    
    public function getTownhouse() { 
        return view('project.townhouse');
    }
    
}