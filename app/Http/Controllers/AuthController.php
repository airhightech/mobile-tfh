<?php

namespace App\Http\Controllers;

use Socialite;
use App\Services\UserProvider;
use Auth;

class AuthController extends Controller {
    
    public function getLogin() {  
        
        return view('auth.login');    
        
    }
    
    public function redirectToFacebook() {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback() {
        $fbuser = Socialite::driver('facebook')->user();

        $user = UserProvider::findOrCreateFromFB($fbuser);

        Auth::login($user, false);

        return redirect('/');
    }
    
}