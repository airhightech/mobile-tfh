<?php

namespace App\Http\Controllers;

class PropertyController extends Controller {
    
    public function search() {          
        return view('property.search'); 
    }
    
    public function getApartment() {
        return view('property.apartment');
    }
    
    public function getCondo() {  
        return view('property.condo'); 
    }
    
    public function getHouse() { 
        return view('property.house');
    }
    
    public function getTownhouse() { 
        return view('property.townhouse');
    }
    
}