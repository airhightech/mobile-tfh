<?php

namespace App\Http\Controllers;


class AgentController extends Controller {
    
    public function getProfile($id) {
        return view('agent.profile', ['id' => $id]);
    }
    
    public function getFind() {
        return view('agent.find'); 
    }
    
}