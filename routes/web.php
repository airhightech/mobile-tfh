<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index');

Route::get('/login', 'AuthController@getLogin');
Route::get('/facebook-login', 'AuthController@redirectToFacebook');
Route::get('/callback-facebook', 'AuthController@handleFacebookCallback');

Route::get('/user/page', 'UserController@getPage');
Route::get('/user/favorites', 'UserController@getFavorites');

Route::get('/agent/profile/{id}', 'AgentController@getProfile');
Route::get('/agent/find', 'AgentController@getFind');

Route::get('/search', 'PropertyController@search');

Route::get('/property/apartment', 'PropertyController@getApartment');
Route::get('/property/condo', 'PropertyController@getCondo');
Route::get('/property/house', 'PropertyController@getHouse');
Route::get('/property/townhouse', 'PropertyController@getTownhouse');

Route::get('/project/apartment', 'ProjectController@getApartment');
Route::get('/project/condo', 'ProjectController@getCondo');
Route::get('/project/house', 'ProjectController@getHouse');
Route::get('/project/townhouse', 'ProjectController@getTownhouse');