@extends('layouts.auth')

@section('content')
<div class="login-container">
    <div style="width: 70%;height: calc(100% - 40px);margin: 0 auto 40px;padding: 20px 0;">
        <img style="height:73px;display:block;margin: 11px auto;" src="img/icon-logo.png"></img>
        <button id="fb-login" style="height:34px;width:calc(100% - 22px);margin: 6px 11px;border:none;background-color:#184a6d;color:white;">Sign in with Facebook</button>
        <button id="twitter-login" style="height:34px;width:calc(100% - 22px);margin: 6px 11px;border:none;background-color:#53bbd4;color:white;">Sign in with Twitter</button>
        <button style="height:34px;width:calc(100% - 22px);margin: 6px 11px;border:none;background-color:#de4b39;color:white;">Sign in with Google+</button>
        <div style="color:#b8b8b8;text-align:center;margin: 11px auto;">OR</div>
        <div style="height:23px;display:block;border-bottom:1px solid #b8b8b8;margin: 22px 0;color: #b8b8b8;padding-left:5px;">
            <div style="display:inline-block;height: 100%;">
                <img style="height:70%;filter: contrast(0%);" src="img/user.png"></img>
            </div>
            <div style="display:inline-block;padding-left:10px;">
                <input type="email" style="border:none;outline:none;" placeholder="E-mail" />
            </div>
        </div>
        <div style="height:23px;display:block;border-bottom:1px solid #b8b8b8;margin: 22px 0;color: #b8b8b8;padding-left:5px;">
            <div style="display:inline-block;height: 100%;">
                <img style="height:70%;filter: contrast(0%);" src="img/padlock.png"></img>
            </div>
            <div style="display:inline-block;padding-left:10px;">
                <input type="password" style="border:none;outline:none;" placeholder="Password" />
            </div>
        </div>
        <button style="height:32px;width:calc(100% - 22px);margin: 7px 11px;border:none;background-color:#4ec9f2;color:white;">Log in</button>
        <div style="font-size:12px;color:#4ec9f2;margin: 10px auto; text-align:center;">
            <a>Create an account</a>
        </div>
        <div style="font-size:12px;color:#4ec9f2;margin: 10px auto; text-align:center;">
            <a>Forgot password</a>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function(){
        $('#fb-login').on('click', function(){
            window.location = '/facebook-login';
        });
        $('#twitter-login').on('click', function(){
            window.location = '/facebook-login';
        });
        $('#google-login').on('click', function(){
            window.location = '/google-login';
        });
    });
    </script>
@endsection