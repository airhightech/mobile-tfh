@extends('layouts.default')

@section('content')
    
    <div class="main-menu">
        <input type="text" id="q" class="search-box" placeholder="Enter District, Area, BTS Station, or Property Name">
        </input>
        <div class="options">
            <div class="row">
                <div class="col-xs-6 menu-item" id="option">
                    Option
                </div>
                <div class="col-xs-6 menu-item" id="view">
                    List View
                </div>
            </div>
        </div>
    </div>

    <div id="mapview" style="width:100%;height:calc(100% - 84px);box-shadow: inset 0px 7px 7px -7px rgba(0,0,0,0.5);">
        
    </div>

    <div class="options-menu">
        <div class="row">
            <div class="col-xs-5">
                <label for="listing">Listing Type</label>
            </div>
            <div class="col-xs-7">
                <select name="listing" id="lt">
                    <option value="rent" selected>Rent</option>
                    <option value="sale">Sale</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="home">Home Type</label>
            </div>
            <div class="col-xs-7">
                <select name="home" id="pt">
                    <option value="co" selected>Condominium</option>
                    <option value="th" >Townhouse</option>
                    <option value="pa" >Apartment</option>
                    <option value="dh" >Detached House</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="bed">Bedrooms</label>
            </div>
            <div class="col-xs-7">
                <select name="bed" id="bed">
                    <option value="studio">Studio</option>
                    <option value="1">1</option>
                    <option value="2" selected>2</option>
                    <option value="3">3</option>
                    <option value="4+">4+</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="bath">Bathrooms</label>
            </div>
            <div class="col-xs-7">
                <select name="bath" id="bath">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2" selected>2</option>
                    <option value="3">3</option>
                    <option value="4+">4+</option>
                </select>
            </div>
        </div>
        <hr style="border-top-width:2px;margin:15px;" />
        <div class="row">
            <div class="col-xs-5">
                <label for="price">Price</label>
            </div>
            <div class="col-xs-7">
                <input type="number" id="pmin" name="pmin" value="" placeholder="Min price" />
                    - 
                <input type="number" id="pmax" name="pmax" value="" placeholder="Max price" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="size">Size</label>
            </div>
            <div class="col-xs-7">
                <input type="number" id="smin" name="smin" value="" placeholder="Min size" />
                    - 
                <input type="number" id="smax" name="smax" value="" placeholder="Max size" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-5">
                <label for="year">Year Built</label>
            </div>
            <div class="col-xs-7">
                <input type="number" id="ymin" name="ymin" value="" placeholder="Min year" />
                    - 
                <input type="number" id="ymax" name="ymax" value="" placeholder="Max year" />
            </div>
        </div>
        <div class="row apply">
            <div class="col-xs-12">
                <button class="apply-search-option">APPLY</button>
            </div>
        </div>
    </div>
    
    

    <div id="listview" style="display:none;width:100%;height:calc(100% - 84px);box-shadow: inset 0px 7px 7px -7px rgba(0,0,0,0.5);">
        List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View List View
    </div>

    <div class="bottom-navigation">
        <a href="javascript:history.back()" style="color:white;"><div class="glyphicon glyphicon-arrow-left"></div></a>
        <a href="/" style="color:white;"><div class="glyphicon glyphicon-home"></div></a>
    </div>

<div id="property-template" style="display: none;">
    <div class="property-item" data-year="" data-price="" data-beds="" data-baths="" data-size="" data-rental="">
        <img src="" class="prop-image img-responsive"/>
        <span class="img-count">2 images</span>
        <button class="favorite-btn"><i class="favorite fa"></i></button>
        <div class="details">
            <span class="listing-type"><i class="fa fa-circle"></i></span>  <span class="property"></span><br/>
            ‎<span class="listing-price"></span><br/>
            <div class="summary">
                <span class="prop-only">
                    <span class="beds"></span> <i class="fa fa-bed" aria-hidden="true"></i> |
                    <span class="bath"></span> <i class="fa fa-bath" aria-hidden="true"></i> |
                    <span class="size"></span>sqm | 
                </span>
                <span class="address"></span>
            </div>
        </div>
    </div>
</div>
    @endsection
    
    @section('scripts')
    <link href="/js/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>
    
    <script src="/lib/jquery/dist/jquery.js"></script>
    <script src="/js/jquery-ui/jquery-ui.min.js"></script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
    <script>
        $(function() {
            $(".menu-item").click(function() {
                if($(this).hasClass("selected")) {
                    $(this).removeClass("selected");
                } else {
                    $(this).addClass("selected");
                }
            });
            $("#option").click(function() {
                if($(this).hasClass("selected")) {
                    $(".options-menu").css("display", "block");
                } else {
                    $(".options-menu").css("display", "none");
                }
            });
            $("#view").click(function() {
                if($(this).hasClass("selected")) {
                    $("#view").text("Map View");
                    $("#mapview").css("display", "none");
                    $("#listview").css("display", "block");
                } else {
                    $("#view").text("Map View");
                    $("#mapview").css("display", "block");
                    $("#listview").css("display", "none");
                }
            });
        });
    </script>
    <script src="/js/infobox_packed.js"></script>
    <script src="/js/markerclusterer.js"></script>
    <script src="/js/property.js"></script>
    <script src="/js/search.js"></script>
@endsection