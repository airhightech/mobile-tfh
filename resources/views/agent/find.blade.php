@extends('layouts.agent')

@section('content')
    <script src="/lib/jquery/dist/jquery.js"></script>
    <div class="main-menu">
        <input type="text" class="search-box" placeholder="Enter a location, a freelance agent or a real estate agency name">
        </input>
        <div class="options">
            <div class="row">
                <div class="col-xs-6 menu-item selected" id="freelance">
                    Freelance Agent
                </div>
                <div class="col-xs-6 menu-item" id="realestate">
                    Real Estate Agency
                </div>
            </div>
        </div>
    </div>

    <div id="freelancepage" style="width:100%;height:calc(100% - 84px);box-shadow: inset 0px 7px 7px -7px rgba(0,0,0,0.5);padding:10px 20px;background-color:#DDEDED;overflow-y:auto;">
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/agent.jpg);background-size:cover;background-position:center;"></div>
                    <div class="agent-search-verified">
                        <div class="glyphicon glyphicon-ok"></div>Verified
                    </div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Phiphat Lohrujinunt</div>
                    <div style="color:gray;font-size:9px;">(10 postings)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/agent.jpg);background-size:cover;background-position:center;"></div>
                    <div class="agent-search-verified">
                        <div class="glyphicon glyphicon-ok"></div>Verified
                    </div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Phiphat Lohrujinunt</div>
                    <div style="color:gray;font-size:9px;">(10 postings)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/agent.jpg);background-size:cover;background-position:center;"></div>
                    <div class="agent-search-verified">
                        <div class="glyphicon glyphicon-ok"></div>Verified
                    </div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Phiphat Lohrujinunt</div>
                    <div style="color:gray;font-size:9px;">(10 postings)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/agent.jpg);background-size:cover;background-position:center;"></div>
                    <div class="agent-search-verified">
                        <div class="glyphicon glyphicon-ok"></div>Verified
                    </div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Phiphat Lohrujinunt</div>
                    <div style="color:gray;font-size:9px;">(10 postings)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/agent.jpg);background-size:cover;background-position:center;"></div>
                    <div class="agent-search-verified">
                        <div class="glyphicon glyphicon-ok"></div>Verified
                    </div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Phiphat Lohrujinunt</div>
                    <div style="color:gray;font-size:9px;">(10 postings)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="realestatepage" style="display:none;width:100%;height:calc(100% - 84px);box-shadow: inset 0px 7px 7px -7px rgba(0,0,0,0.5);padding:10px 20px;background-color:#DDEDED;overflow-y:auto;">
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/realestate.jpg);background-size:cover;background-position:center;"></div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Top Broker Co., Ltd.</div>
                    <div style="color:gray;font-size:9px;">(230 houses)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/realestate.jpg);background-size:cover;background-position:center;"></div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Top Broker Co., Ltd.</div>
                    <div style="color:gray;font-size:9px;">(230 houses)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/realestate.jpg);background-size:cover;background-position:center;"></div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Top Broker Co., Ltd.</div>
                    <div style="color:gray;font-size:9px;">(230 houses)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/realestate.jpg);background-size:cover;background-position:center;"></div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Top Broker Co., Ltd.</div>
                    <div style="color:gray;font-size:9px;">(230 houses)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
        <div style="width:100%;height:120px;box-shadow: 2px 3px 5px 0px rgba(0,0,0,0.3);background-color:white;margin-bottom:15px;">
            <div class="row" style="height:100%;padding:10px 8px;">
                <div class="col-xs-3" style="height:100%;padding-right:0;">
                    <div style="width: 100%;height:75px;background-image: url(/img/realestate.jpg);background-size:cover;background-position:center;"></div>
                </div>
                <div class="col-xs-9">
                    <div style="color:#085978;font-weight:bold;font-size:13px;">Top Broker Co., Ltd.</div>
                    <div style="color:gray;font-size:9px;">(230 houses)</div>
                    <div style="height:12px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Expert : </div> <div style="display:inline-block;font-size:9px;"> House, Land, Townhouse</div></div>
                    <div style="height:18px;"><div style="display:inline-block;color:#4ec9f2;font-weight:bold;font-size:9px;">Review : </div> <div style="display:inline-block;font-size:9px;"> 4 reviews</div></div>
                    <div style="font-size:9px;">I work with property buyers or sellers and help them navigate the complex nature of the ...</div>
                    <div style="float:right;"><button style="height:20px;width:50px;border:none;color:white;background-color:#37A8D4;font-size:9px;">Contact</button></div>
                    <div style="position:absolute;top:0px;right:15px;">
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                        <span><div style="color:#18AEC5;" class="glyphicon glyphicon-star"></div></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-navigation">
        <a href="javascript:history.back()" style="color:white;"><div class="glyphicon glyphicon-arrow-left"></div></a>
        <a href="/" style="color:white;"><div class="glyphicon glyphicon-home"></div></a>
    </div>

    <script>
        $(function() {
            $("#freelance").click(function() {
                if(!$(this).hasClass("selected")) {
                    $(this).addClass("selected");
                    $("#realestate").removeClass("selected");
                    $("#freelancepage").css("display", "block");
                    $("#realestatepage").css("display", "none");
                }
            });
            $("#realestate").click(function() {
                if(!$(this).hasClass("selected")) {
                    $(this).addClass("selected");
                    $("#freelance").removeClass("selected");
                    $("#freelancepage").css("display", "none");
                    $("#realestatepage").css("display", "block");
                }
            });
        });
    </script>
@endsection