@extends('layouts.agent')

@section('content')
    <link rel="stylesheet" type="text/css" href="/lib/bootstrap/dist/css/bootstrap.min.css">
    <script src="/lib/angular/angular.js"></script>
    <script src="/lib/angular-animate/angular-animate.js"></script>
    <script src="/lib/angular-bootstrap/ui-bootstrap-tpls.js"></script>
    <script src="/lib/lodash/lodash.js"></script>
    <script src="/lib/chart.js/dist/Chart.js"></script>
    <script src="/lib/angular-chart.js/dist/angular-chart.js"></script>
    <script src="/lib/jquery/dist/jquery.js"></script>

    <div ng-app="myApp" ng-controller="myController">
        <div class="agent-header">
            <div class="agent-logo-container" ng-attr-back-img="@{{cover ? cover : undefined}}">
                <div class="agent-circle" ng-attr-back-img="@{{profile ? profile : undefined}}">
                </div>
            </div>
            <div class="agent-title">
                <div>
                    <div class="agent-badge glyphicon glyphicon-user"></div>
                    <span class="agent-name">@{{agent.name}}</span>
                </div>
                <div style="margin-bottom:10px;">
                    <div ng-if="true">
                        <div class="agent-search-verified" style="float:left;margin-left: calc(50% - 70px);margin-top:0;margin-right:25px;">
                            <div class="glyphicon glyphicon-ok"></div>Verified
                        </div>
                    </div>
                    <div class="agent-posting">(Posting @{{agent.posts}})</div>
                </div>
                <div style="width:80px;height:1px;margin-left:calc(50% - 40px);font-size:16px;color:gold;text-align:center;">
                    <div ng-repeat="i in getNumber(agent.ranking) track by $index" style="float:left;"><div class="glyphicon glyphicon-star"></div></div>
                </div>
            </div>
            <div class="agent-tabs">
                <div class="agent-tab selected">
                    About
                </div>
                <div class="agent-tab middle">
                    Items
                </div>
                <div class="agent-tab">
                    Review
                </div>
            </div>
            <div class="agent-tab-view about">
                <div class="row">
                    <div class="col-xs-5 gray">
                        Agent name
                    </div>
                    <div class="col-xs-7 black">
                        @{{agent.name}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5 gray">
                        Agent ID
                    </div>
                    <div class="col-xs-7 black">
                        @{{agent.id}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5 gray">
                        E-mail
                    </div>
                    <div class="col-xs-7 black">
                        @{{agent.email}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5 gray">
                        Tel
                    </div>
                    <div class="col-xs-7 black">
                        @{{agent.tel}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5 gray">
                        Website
                    </div>
                    <div class="col-xs-7 black">
                        @{{agent.website}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-5 gray">
                        Language ability
                    </div>
                    <div class="col-xs-7 black">
                        <span ng-repeat="lang in agent.langs">@{{lang}}@{{$last ? '' : ', '}}</span>
                    </div>
                </div>
            </div>
            <div class="agent-tab-view items">
                <div class="overview">
                    <div class="col-xs-7 left">
                        <canvas id="doughnut" class="chart chart-doughnut" chart-data="data" chart-labels="labels" chart-colors="backgroundColor"></canvas>
                        <div style="width:100%;padding:30px 0;font-size:10px;">
                            <div style="display:flex;height:30px;">
                                <div ng-style="{ flex: round(sale * 100 / (sale + rent))}" style="background-color:#d2f7ff;text-align:center;line-height:30px;">
                                    @{{round(sale * 100 / (sale + rent)) + '%'}}
                                </div>
                                <div ng-style="{ flex: round(rent * 100 / (sale + rent))}" style="background-color:#1191AE;text-align:center;color:white;line-height:30px;">
                                    @{{round(rent * 100 / (sale + rent)) + '%'}}
                                </div>
                            </div>
                            <div style="float:left;font-weight:bold;color:black;">Sale</div><div style="float:right;font-weight:bold;color:black;">Rent</div>
                        </div>
                    </div>
                    <div class="col-xs-5 right">
                        <div class="onefourth house">
                            <div class="col-xs-5" style="height:100%;">
                                <img src="/img/detachedhouse.png" style="height:80%;filter:invert(100%);"></img>
                            </div>
                            <div class="col-xs-7" style="height:100%;padding: 5px 0;">
                                <div>Single House</div>
                                <div>@{{house}}</div>
                            </div>
                        </div>
                        <div class="onefourth townhouse">
                            <div class="col-xs-5" style="height:100%;">
                                <img src="/img/townhouse.png" style="height:80%;filter:invert(100%);"></img>
                            </div>
                            <div class="col-xs-7" style="height:100%;padding: 5px 0;">
                                <div>Townhouse</div>
                                <div>@{{townhouse}}</div>
                            </div>
                        </div>
                        <div class="onefourth apartment">
                            <div class="col-xs-5" style="height:100%;">
                                <img src="/img/apartment.png" style="height:80%;filter:invert(100%);"></img>
                            </div>
                            <div class="col-xs-7" style="height:100%;padding: 5px 0;">
                                <div>Apartment</div>
                                <div>@{{apartment}}</div>
                            </div>
                        </div>
                        <div class="onefourth condo">
                            <div class="col-xs-5" style="height:100%;">
                                <img src="/img/condo.png" style="height:80%;filter:invert(100%);"></img>
                            </div>
                            <div class="col-xs-7" style="height:100%;padding: 5px 0;">
                                <div>Condominium</div>
                                <div>@{{condominium}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="listing-header">
                    <div class="list-item">
                        <button class="list-button selected">Rent</button>
                    </div>
                    <div class="list-item">
                        <button class="list-button">Buy</button>
                    </div>
                    <div class="list-item" style="float:right;">
                        <select class="list-select">
                            <option>Property Type</option>
                        </select>
                    </div>
                    <div class="list-item" style="float:right;margin-right:2px;">
                        <select class="list-select">
                            <option>Listing Type</option>
                        </select>
                    </div>
                </div>
                <div class="list-items">
                    <div ng-repeat="property in properties">
                        <div ng-init="property.index = 0;" style="display:none;"></div>
                        <h2 style="font-size:14px;margin:5px auto;">@{{property.property_name ? property.property_name : 'No title'}}</h2>
                        <p style="padding:0;font-size:13px;">@{{property.details.address_en ? property.details.address_en : 'No address'}}</p>
                        <div style="display:flex;color:white;margin-bottom:15px;">
                            <div style="position:relative;flex:1;height:120px;" back-img="@{{property.images[property.index]}}">
                                <div ng-show="property.images.length > 0" style="z-index:2;position:absolute;height:100%;width:20%;top:0;left:0;cursor:pointer;" ng-click="decIndex($index)">
                                    <img src="/img/left-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
                                </div>
                                <div ng-show="property.images.length > 0" style="z-index:2;position:absolute;height:100%;width:20%;top:0;right:0;cursor:pointer;" ng-click="incIndex($index)">
                                    <img src="/img/right-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
                                </div>
                                <div style="z-index:3;text-align:center;position:absolute;height:25px;line-height:25px;width:25px;top:5px;left:5px;font-size:13px;background:linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3));border-radius:50%;padding-top:2px;padding-right:2px;cursor:pointer;" ng-click="favorite($index)" ng-attr-id="@{{'favorite' + $index}}">
                                    <div class="glyphicon glyphicon-heart"></div>
                                </div>
                                <div style="z-index:1;text-align:center;position:absolute;height:18px;line-height:18px;width:45px;bottom:0;left:0;background:linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6));font-size:10px;">
                                    @{{property.images.length > 0 ? (property.index + 1) + ' of ' + property.images.length : 'NoImage'}}
                                </div>
                            </div>
                            <div style="flex:1;height:120px;padding:10px;" back-img-blur="@{{property.images[((property.index + 1) % property.images.length)]}}">
                                <div style="margin-top:50px;font-size:8px;">
                                    <p style="margin:0;">@{{property.property_name ? property.property_name : 'No title'}}</p>
                                </div>
                                <div style="display:flex;align-items: center;font-size:8px;width:100%;">
                                    <p style="flex:42;margin:0 auto;font-size:14px;font-weight:bold;">&#3647; @{{property.details.listing_price ? property.details.listing_price : '-'}}</p>
                                    <p style="flex:18;margin:0 auto;">| @{{property.details.bedroom_num ? property.details.bedroom_num + ' Beds' : '-'}}</p>
                                    <p style="flex:18;margin:0 auto;"> @{{property.details.bedroom_num ? property.details.bedroom_num + ' Baths' : '-'}}</p>
                                    <p style="flex:22;margin:0 auto;">| @{{property.details.total_size ? property.details.total_size + ' sqm' : '-'}}</p>
                                </div>
                                <div style="font-size:8px;">
                                    <p style="">@{{property.details.address_en ? property.details.address_en : 'No address'}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="agent-tab-view review">
                <div class="agent-comments">
                    <div style="width:100%;">
                        <p style="display:table;margin: 10px 10px 0;text-transform:uppercase;font-weight:bold;font-size:12px;">Write Review</p>
                        <div style="font-size:10px;padding: 10px;" class="agent-commenting">
                            <div style="clear:both;height:10px;">
                                <div style="float:left;height:20px;">Give us your recommendation</div>
                                <div style="float:left;margin-top:-11px;margin-left: 30px;height:20px;">
                                    <fieldset class="rating-blue">
                                        <input type="radio" ng-click="reply.rating = 10" id="star51" name="rating1" value="5" /><label class = "full" for="star51" title="Awesome - 5 stars"></label>
                                        <input type="radio" ng-click="reply.rating = 9" id="star41half" name="rating1" value="4 and a half" /><label class="half" for="star41half" title="Pretty good - 4.5 stars"></label>
                                        <input type="radio" ng-click="reply.rating = 8" id="star41" name="rating1" value="4" /><label class = "full" for="star41" title="Pretty good - 4 stars"></label>
                                        <input type="radio" ng-click="reply.rating = 7" id="star31half" name="rating1" value="3 and a half" /><label class="half" for="star31half" title="Meh - 3.5 stars"></label>
                                        <input type="radio" ng-click="reply.rating = 6" id="star31" name="rating1" value="3" /><label class = "full" for="star31" title="Meh - 3 stars"></label>
                                        <input type="radio" ng-click="reply.rating = 5" id="star21half" name="rating1" value="2 and a half" /><label class="half" for="star21half" title="Kinda bad - 2.5 stars"></label>
                                        <input type="radio" ng-click="reply.rating = 4" id="star21" name="rating1" value="2" /><label class = "full" for="star21" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" ng-click="reply.rating = 3" id="star11half" name="rating1" value="1 and a half" /><label class="half" for="star11half" title="Meh - 1.5 stars"></label>
                                        <input type="radio" ng-click="reply.rating = 2" id="star11" name="rating1" value="1" /><label class = "full" for="star11" title="Sucks big time - 1 star"></label>
                                        <input type="radio" ng-click="reply.rating = 1" id="starhalf1" name="rating1" value="half" /><label class="half" for="starhalf1" title="Sucks big time - 0.5 stars"></label>
                                    </fieldset>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <p>Describe in detail your experience with this agent</p>
                            <textarea placeholder="Your comment" style="resize:none;width:100%;height:60px;" ng-model="reply.user.message"></textarea>
                            <br/>
                            <div style="height:30px;">
                                <button class="btn" style="float:right;width:110px;background-color:#9d9d9d;font-size:10px;height:30px;" ng-click="sendReview()">Send The Review</button>
                                <!--
                                <div style="display:inline-block;font-size:20px;vertical-align:middle;" class="glyphicon glyphicon-upload"></div>
                                <button style="border:none;display:inline-block;color: #666e71;height:30px;width:85px;background-color:inherit;">Upload Picture</button>
                                -->
                            </div>
                        </div>
                    </div>
                    <div style="width: 100%;padding: 20px 5px 0px 5px;border-top: 1px solid #44B2CC;font-size:10px;">
                        <div class="agent-comment" ng-repeat="comment in comments.slice(((currentPage - 1) * itemsPerPage), (((currentPage - 1) * itemsPerPage) + itemsPerPage))">
                            <div class="row">
                                <div class="col-xs-2">
                                    <div style="width:100%;padding: 90%;" back-img="@{{comment.user.image}}"></div>
                                </div>
                                <div class="col-xs-10">
                                    <div class="commenter-email">
                                        @{{comment.user.name}}
                                    </div>
                                    <div>
                                        <fieldset class="rating-blue-passive">
                                                <input type="radio" disabled ng-checked="comment.rating == 10" ng-attr-id="@{{$index + 'star51'}}" ng-attr-name="@{{$index + 'rating1'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star51'}}" title="Awesome - 5 stars"></label>
                                                <input type="radio" disabled ng-checked="comment.rating == 9" ng-attr-id="@{{$index + 'star41half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star41half'}}" title="Pretty good - 4.5 stars"></label>
                                                <input type="radio" disabled ng-checked="comment.rating == 8" ng-attr-id="@{{$index + 'star41'}}" ng-attr-name="@{{$index + 'rating1'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star41'}}" title="Pretty good - 4 stars"></label>
                                                <input type="radio" disabled ng-checked="comment.rating == 7" ng-attr-id="@{{$index + 'star31half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star31half'}}" title="Meh - 3.5 stars"></label>
                                                <input type="radio" disabled ng-checked="comment.rating == 6" ng-attr-id="@{{$index + 'star31'}}" ng-attr-name="@{{$index + 'rating1'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star31'}}" title="Meh - 3 stars"></label>
                                                <input type="radio" disabled ng-checked="comment.rating == 5" ng-attr-id="@{{$index + 'star21half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star21half'}}" title="Kinda bad - 2.5 stars"></label>
                                                <input type="radio" disabled ng-checked="comment.rating == 4" ng-attr-id="@{{$index + 'star21'}}" ng-attr-name="@{{$index + 'rating1'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star21'}}" title="Kinda bad - 2 stars"></label>
                                                <input type="radio" disabled ng-checked="comment.rating == 3" ng-attr-id="@{{$index + 'star11half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star11half'}}" title="Meh - 1.5 stars"></label>
                                                <input type="radio" disabled ng-checked="comment.rating == 2" ng-attr-id="@{{$index + 'star11'}}" ng-attr-name="@{{$index + 'rating1'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star11'}}" title="Sucks big time - 1 star"></label>
                                                <input type="radio" disabled ng-checked="comment.rating == 1" ng-attr-id="@{{$index + 'starhalf1'}}" ng-attr-name="@{{$index + 'rating1'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf1'}}" title="Sucks big time - 0.5 stars"></label>
                                            </fieldset>
                                        </div>
                                        <div style="margin-top: 11px;margin-left: 145px;">(2 days ago)</div>
                                    <div style="margin:20px 0 20px 0;border-bottom: 1px solid #44B2CC;padding-bottom:20px;">
                                        @{{comment.user.message}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="comments-end" style="display: table;margin: 20px auto;" items-per-page="itemsPerPage" uib-pagination boundary-links="true" max-size="maxSize" rotate="true" total-items="totalItems" ng-model="currentPage" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></div>
                    </div>
                </div>
            </div>
            <div style="width:100%;padding: 0 20px;">
                <div class="agent-contact">
                    <p>Contact</p>
                    <form name="form">
                        <label for="name">Name</label><input name="name" type="text" placeholder="Name" ng-model="contact.name" />
                        <label for="tel">Tel.</label><input name="tel" type="tel" placeholder="Telephone" ng-model="contact.phone" />
                        <label for="email">E-mail</label><input name="email" type="email" placeholder="E - mail" ng-model="contact.email" />
                        <label for="message">Message</label><textarea name="message" placeholder="Your message" ng-model="contact.message"></textarea>
                        <input class="news" name="news" type="checkbox" ng-model="contact.newsletter" /><label class="news" for="news">You want us to send News Letters</label>
                        <button class="submit btn btn-primary" name="submit" type="submit" ng-click="submitMessage()"><i class="glyphicon glyphicon-ok"></i>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom-navigation">
        <a href="javascript:history.back()" style="color:white;"><div class="glyphicon glyphicon-arrow-left"></div></a>
        <a href="/" style="color:white;"><div class="glyphicon glyphicon-home"></div></a>
    </div>

    <script src="/agent.js"></script>
    <script>
        var agentId = {{ $id }};
        $(function() {
            $(".agent-tab").click(function() {
                if(!$(this).hasClass("selected")) {
                    $(".agent-tab").removeClass("selected");
                    $(this).addClass("selected");
                    $(".agent-tab-view").css("display", "none");
                    $("." + $(this).text().toLowerCase().trim()).css("display", "block");
                }
            });
            $(".list-button").click(function() {
                if($(this).hasClass("selected")) {
                    $(this).removeClass("selected");
                } else {
                    $(this).addClass("selected");
                }
            });
        });
    </script>
@endsection