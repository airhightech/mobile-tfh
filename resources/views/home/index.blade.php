@extends('layouts.default')

@section('content')
    <link rel="stylesheet" type="text/css" href="/lib/bootstrap/dist/css/bootstrap.min.css">
    <script src="/lib/angular/angular.js"></script>
    <div class="body-container" ng-app="myApp" ng-controller="myController">
        <div style="width:100%;height:calc(100% - 63px);padding:9%;background:rgba(0, 0, 0, 0.53);background-image: linear-gradient(rgba(4, 62, 71, 0.53), rgba(4, 62, 71, 0.53)), url('/img/back.jpg');background-size: cover;" ng-if="selectedTab == 0">
            <form style="width:100%;height:100%;" id="inner-main" ng-submit="search()">
                <button type="button" class="big-button selected" ng-click="selectButton('rent')" id="rent">Rent</button>
                <div style="height:1px;float:left;" class="spacers"></div>
                <button type="button" class="big-button" ng-click="selectButton('buy')" id="buy">Buy</button>
                <button type="button" class="big-button" ng-click="selectButton('condo')" id="condo">Condo Community</button>
                <div style="height:1px;float:left;" class="spacers"></div>
                <button type="button" class="big-button" ng-click="selectButton('new')" id="new">New Project</button>
                <input type="text" class="search-box" placeholder="Type name or area" id="search" />
            </form>
        </div>
        <div style="width:100%;height:calc(100% - 63px);padding:7%;border-top:3px solid #dbdbdb;overflow-y:auto;" ng-if="selectedTab == 1">
            <div style="height:100%;width:100%;">
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image"></div>
                    <div class="listing-details">
                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">Sansiri home modern Luxury</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;padding: 0 2px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(60% - 10px);font-size:9px;">Asoke</div>
                            <div style="display:inline-block;width:35%;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; 1,0xx,xxx</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">12/336 XXX Road XXX XXXXXXXX Bangkok 10400</div>
                    </div>
                </div>
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image"></div>
                    <div class="listing-details">
                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">Sansiri home modern Luxury</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;padding: 0 2px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(60% - 10px);font-size:9px;">Asoke</div>
                            <div style="display:inline-block;width:35%;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; 1,0xx,xxx</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">12/336 XXX Road XXX XXXXXXXX Bangkok 10400</div>
                    </div>
                </div>
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image"></div>
                    <div class="listing-details">
                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">Sansiri home modern Luxury</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;padding: 0 2px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(60% - 10px);font-size:9px;">Asoke</div>
                            <div style="display:inline-block;width:35%;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; 1,0xx,xxx</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">12/336 XXX Road XXX XXXXXXXX Bangkok 10400</div>
                    </div>
                </div>
                <!-- old design
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image"></div>
                    <div class="listing-details">
                        <div style="color:#000000;font-size:12px;">Home aaaa aaaaa aaaaa</div>
                        <div style="display:block;width:100%;color:#006979">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;margin-top:-3px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(55% - 10px);font-size:12px;">Pattaya Beach</div>
                            <div style="display:inline-block;width:40%;text-align:right;">1XXXXXXX THB</div>
                        </div>
                    </div>
                </div>
                -->
            </div>
        </div>
        <div style="width:100%;height:calc(100% - 63px);padding:7%;border-top:3px solid #dbdbdb;overflow-y:auto;" ng-if="selectedTab == 2">
            <div style="height:100%;width:100%;">
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image" style="background-image: url(/img/inside.jpg);"></div>
                    <div class="listing-details">
                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">BBB condo Phayathai</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;padding: 0 2px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(60% - 10px);font-size:9px;">Asoke</div>
                            <div style="display:inline-block;width:35%;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; 1,0xx,xxx</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">12/336 XXX Road XXX XXXXXXXX Bangkok 10400</div>
                    </div>
                </div>
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image" style="background-image: url(/img/inside.jpg);"></div>
                    <div class="listing-details">
                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">BBB Home</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;padding: 0 2px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(60% - 10px);font-size:9px;">Asoke</div>
                            <div style="display:inline-block;width:35%;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; 1,0xx,xxx</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">12/336 XXX Road XXX XXXXXXXX Bangkok 10400</div>
                    </div>
                </div>
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image" style="background-image: url(/img/inside.jpg);"></div>
                    <div class="listing-details">
                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">BBB condo Phayathai</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;padding: 0 2px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(60% - 10px);font-size:9px;">Asoke</div>
                            <div style="display:inline-block;width:35%;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; 1,0xx,xxx</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">12/336 XXX Road XXX XXXXXXXX Bangkok 10400</div>
                    </div>
                </div>
                <!-- old design 2
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image" style="background-image: url(/img/inside.jpg);"></div>
                    <div class="listing-details">
                        <div style="color:#000000;font-size:12px;">Bbb Condo</div>
                        <div style="display:block;width:100%;color:#006979">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;margin-top:-3px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(55% - 10px);font-size:12px;">Phayathai</div>
                            <div style="display:inline-block;width:40%;text-align:right;">1XXXXXXX THB</div>
                        </div>
                    </div>
                </div>
                -->
            </div>
        </div>
        <div style="width:100%;height:calc(100% - 63px);padding:7%;border-top:3px solid #dbdbdb;overflow-y:auto;" ng-if="selectedTab == 3">
            <div style="height:100%;width:100%;">
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image" style="background-image: url(/img/villa.jpg);"></div>
                    <div class="listing-details">
                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">Blue Sky house</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;padding: 0 2px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(60% - 10px);font-size:9px;">Asoke</div>
                            <div style="display:inline-block;width:35%;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; 1,0xx,xxx</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">12/336 XXX Road XXX XXXXXXXX Bangkok 10400</div>
                    </div>
                </div>
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image" style="background-image: url(/img/villa.jpg);"></div>
                    <div class="listing-details">
                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">XXX house</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;padding: 0 2px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(60% - 10px);font-size:9px;">Asoke</div>
                            <div style="display:inline-block;width:35%;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; 1,0xx,xxx</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">12/336 XXX Road XXX XXXXXXXX Bangkok 10400</div>
                    </div>
                </div>
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image" style="background-image: url(/img/villa.jpg);"></div>
                    <div class="listing-details">
                        <div style="display:block;width:100%;color:#000000;font-size:12px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/home.png" style="width:100%;margin-top:-4px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(95% - 10px);font-size:12px;">Blue Sky house</div>
                        </div>
                        <div style="display:block;width:100%;color:#000000;margin-top:-7px;">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;padding: 0 2px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(60% - 10px);font-size:9px;">Asoke</div>
                            <div style="display:inline-block;width:35%;text-align:right;font-weight:bold;font-size:16px;color:#BB121E;">&#3647; 1,0xx,xxx</div>
                        </div>
                        <div style="font-size:9px;margin-top:-1px;">12/336 XXX Road XXX XXXXXXXX Bangkok 10400</div>
                    </div>
                </div>
                <!-- old design 3
                <div class="listing-item">
                    <div class="exclusive-label"></div>
                    <div class="exclusive-label-text">Exclusive</div>
                    <div class="listing-image" style="background-image: url(/img/villa.jpg);"></div>
                    <div class="listing-details">
                        <div style="color:#000000;font-size:12px;">Blue Home</div>
                        <div style="display:block;width:100%;color:#006979">
                            <div style="display:inline-block;width:5%;">
                                <img src="/img/placeholder.png" style="width:100%;margin-top:-3px;"></img>
                            </div>
                            <div style="display:inline-block;width:calc(55% - 10px);font-size:12px;">Phatumwan</div>
                            <div style="display:inline-block;width:40%;text-align:right;">1XXXXXXX THB</div>
                        </div>
                    </div>
                </div>
                -->
            </div>
        </div>
        <div style="width:100%;height:63px;background-color:white;display:flex;text-transform:uppercase;text-align: center;color: #4ec9f2;font-weight:bold;">
            <div class="home-tab" ng-click="selectTab(0)" id="tab0">
                <div class="tab-mark"></div>
                <div style="position: absolute;top: 50%;left: 50%;transform: translateX(-50%) translateY(-50%);width:100%;">Exclusive Project</div>
            </div>
            <div class="home-tab" ng-click="selectTab(1)" id="tab1">
                <div class="tab-mark"></div>
                <div style="position: absolute;top: 50%;left: 50%;transform: translateX(-50%) translateY(-50%);width:100%;">For Rent</div>
            </div>
            <div class="home-tab" ng-click="selectTab(2)" id="tab2">
                <div class="tab-mark"></div>
                <div style="position: absolute;top: 50%;left: 50%;transform: translateX(-50%) translateY(-50%);width:100%;">For Sale</div>
            </div>
        </div>
    </div>
    <script src="/homepage.js"></script>
@endsection