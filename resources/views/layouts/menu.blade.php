<script src="/lib/jquery/dist/jquery.js"></script>
<ul class="dropped">
    <!--
    <li class="lang">
        <span>English</span>
        <button class="btn btn-lang pull-right"><i class="fa fa-chevron-right"></i></button>
    </li>
    -->
    <li class="lang">
        <span class="lang-flag zh"></span>
        <span class="lang-flag ja"></span>
        <span class="lang-flag ko"></span>
        <span class="lang-flag en active"></span>
        <span class="lang-flag th"></span>
    </li>
    <li><a class="btn btn-menu btn-block" href="/property/search?lt=rent">{{ trans('menu.rent') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/property/search?lt=sale">{{ trans('menu.sale') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/property/search?np=yes">{{ trans('menu.new-project') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/property/search?pt=cd">{{ trans('menu.condo-community') }}</a></li>
    <li><a class="btn btn-menu btn-block" href="/agent/find">{{ trans('menu.find-agent') }}</a></li>
    <li style="text-align:center;"><a href="http://beta.thaifullhouse.com/">Go To Desktop Website</a></li>
</ul>

<script>
    $(function() {
        $(".lang-flag").click(function() {
            if(!$(this).hasClass("active")) {
                $(".lang-flag").removeClass("active");
                $(this).addClass("active");
            }
        });
        $('.lang-flag.th').on('click', function(){
            window.location = '/?hl=th';
        });
        $('.lang-flag.en').on('click', function(){
            window.location = '/?hl=en';
        });
        $('.lang-flag.ja').on('click', function(){
            window.location = '/?hl=ja';
        });
        $('.lang-flag.ko').on('click', function(){
            window.location = '/?hl=ko';
        });
        $('.lang-flag.zh').on('click', function(){
            window.location = '/?hl=zh';
        });

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        };

        $(document).ready(function () {
            var lang = getUrlParameter('hl');
            if(lang != undefined) {
                $(".lang-flag").removeClass("active");
                $(".lang-flag." + lang).addClass("active");
            }
        });
    });
</script>
