<div style="background-color: #FFF; margin: 0; width: 300px; height: 400px; padding: 10px;overflow-y:auto;">

    <?php
    if (count($favorites)) {
        foreach ($favorites as $property) {
            $details = $property->getDetails();

            $thumb = $property->getListingThumb();

            $thumb_url = $thumb ? 'http://demo.thaifullhouse.com' . $thumb->getRelativeThumUrl() : '/img/default-property.png';
            ?>
            <div style="display:flex;height:100px;padding:5px;background-color:#e6e6e6;margin-bottom:10px;">
                <div style="flex:1;height:100%;background: url('{{ $thumb_url }}');background-size:cover;background-position:center;margin-right:10px;">
                </div>
                <div style="flex:2;font-size:10px;">
                    <p style="color:#558a9a;font-weight:bold;margin-bottom:2px;">
                        <a href="/property/details/{{ $property->id }}">
                            [{{ strtoupper($property->listing_type) }}] {{ $property->getJsonTranslatedField('name') }}
                        </a>
                    </p>
                    <p style="word-break: break-all;line-height:110%;max-height: 67px; overflow: hidden;">
                        {{ $details->getJsonTranslatedField('other_details') }}
                    </p>
                    <!--            
                    <button style="background-color:#558a9a;color:white;float:right;border:none;font-size:8px;font-weight:bold;padding:4px 7px;">CONTACT</button>
                    -->
                </div>
            </div>
            <?php
        }
    } else {
        ?>
        <div class="alert alert-warning">
            You don't have any favorite property
        </div>
        <?php
    }
    ?>
</div>