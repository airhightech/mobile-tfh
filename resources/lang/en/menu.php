<?php

return [
    'rent' => 'RENT',
    'sale' => 'SALE',
    'new-project' => 'New project',
    'condo-community' => 'Condo community',
    'find-agent' => 'Find agent'
];