(function () {
    'use strict';

    angular
        //angular main modules
        .module('myApp', ['ui.bootstrap', 'chart.js', 'ngAnimate'])
        //controller
        .controller('myController', myController)
        //API service
        .service('agentService', agentService)
        //For giving custom background image to a div dynamically
        .directive('backImg', backImg)
        //For giving custom background image to a div dynamically with tinted view
        .directive('backImgBlur', backImgBlur);

    //dependency injections
    myController.$inject = ['$scope', '$timeout', 'agentService', '$location', '$anchorScroll', '$uibModal'];
    agentService.$inject = ['$http', '$q'];

    //the controller
    function myController($scope, $timeout, agentService, $location, $anchorScroll, $uibModal) {
        //copy Math.round function to use it on html
        $scope.round = Math.round;
        //Make an API call to get agent details and integrate variables
        agentService.getAgentDetails(agentId).then(function(agentDetails) {
            $scope.agent = agentDetails.agent;
            $scope.comments = agentDetails.agent.comments.data;
            $scope.totalItems = agentDetails.agent.comments.data.length;
            $scope.itemsPerPage = agentDetails.agent.comments.per_page;
            $scope.currentPage = agentDetails.agent.comments.current_page;
            $scope.rent = agentDetails.agent.listings.rent;
            $scope.sale = agentDetails.agent.listings.sale;
            $scope.apartment = agentDetails.agent.listings.apartment;
            $scope.condominium = agentDetails.agent.listings.condominium;
            $scope.townhouse = agentDetails.agent.listings.townhouse;
            $scope.house = agentDetails.agent.listings.house;
            $scope.data = [$scope.house, $scope.townhouse, $scope.apartment, $scope.condominium];
            $scope.properties = agentDetails.agent.properties;
            $scope.profile = agentDetails.agent.profile_picture;
            $scope.cover = agentDetails.agent.cover_picture;
            $scope.logo = agentDetails.agent.logo;
            $scope.verified = agentDetails.agent.agent_verified;
        });
        //variable declerations and initializations
        $scope.agent = {
            name: 'Agent Wonder',
            ranking: 3,
            posts: 450,
            id: 'A00001',
            user_id: '1',
            email: 'aaa@aaa.com',
            website: 'www.aaa.com',
            langs: ['English', 'Thai'],
            tel: '05-5555-555',
            address: 'Khlong Chan, Bang Kapi, Bangkok 10240'
        };
        $scope.search = {
            type: 'list',
            index: 0,
            size: 2,
            img: [
                'http://demo.thaifullhouse.com/image/57eca77954ba6-57eca779.jpg',
                'http://demo.thaifullhouse.com/thumb/320x300/57ee03a56bac7-57ee03a5.jpg'
            ],
            favorite: false,
            favorite2: false,
            favorite3: false
        };
        //index function for switching between images in listings
        $scope.decIndex = function(index) {
            if ($scope.properties[index].index > 0) {
                $scope.properties[index].index--;
            }
        };
        $scope.incIndex = function(index) {
            if ($scope.properties[index].index < $scope.properties[index].images.length - 1) {
                $scope.properties[index].index++;
            }
        };
        //add listing to favorites (heart button)
        $scope.favorite = function(index) {
            var color = $('#favorite' + index).css('color');
            $('#favorite' + index).css('color', (color == 'rgb(255, 255, 255)' ? 'rgb(255, 0, 0)' : 'rgb(255, 255, 255)'));
        };
        $scope.comments = [
            {
                rating: 6
            },
            {
                rating: 2
            },
            {
                rating: 10
            },
            {
                rating: 8
            }
        ];
        $scope.reply = {
            rating: 0,
            user: {
                email: 'youremail@website.com',
                name: 'Your Name',
                message: null,
                image: null
            }
        };
        $scope.sale = 60;
        $scope.rent = 140;
        $scope.apartment = 12;
        $scope.condominium = 88;
        $scope.house = 50;
        $scope.townhouse = 50;
        $scope.totalItems = $scope.comments.length;
        $scope.itemsPerPage = 3;
        $scope.currentPage = 1;
        $scope.labels = ['House', 'Townhouse', 'Apartment', 'Condominium'];
        $scope.data = [$scope.house, $scope.townhouse, $scope.apartment, $scope.condominium];
        $scope.backgroundColor = ['#179aed', '#93bcd6', '#cbff7a', '#ff5d83'];
        $scope.getNumber = function (num) {
            return new Array(num);
        };
        //scroll to div with specific id
        $scope.scrollTo = function (id) {
            $location.hash(id);
            $anchorScroll();
        };
        //number functions
        $scope.beautify = function (price) {
            return parseInt(price).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        };
        $scope.formatPrice = function (num) {
            if (num >= 1000000) {
                return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
            }
            if (num >= 1000) {
                return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
            }
            return num;
        };//}};//Don't delete this line, this is visual code coloring bug hack-fix.

        //send review
        $scope.sendReview = function() {
            if (userIsOnline) {
                agentService.sendReview($scope.agent.user_id, $scope.reply.rating, $scope.reply.user.message).then(function() {
                    console.log('sent');
                    $scope.scrollTo('comments-end');
                    $scope.totalItems++;
                    $scope.currentPage = Math.ceil($scope.totalItems / $scope.itemsPerPage);
                    var reply = angular.copy($scope.reply);
                    $scope.reply = {
                        rating: 0,
                        user: {
                            email: 'youremail@website.com',
                            name: 'Your Name',
                            message: null,
                            image: null
                        }
                    };
                    $('#reply-ratings *').removeAttr('checked');
                    $scope.comments.push(reply);
                });
            } else {
                //display message on error
                console.log('not sent');
                var data = {
                    boldTextTitle: '',
                    textAlert: onOfflineMessage,
                    mode: 'info'
                }
                var modal = $scope.open(data);
                $timeout(function () { modal.close(); }, 3000);
            }
        }
        //send message to agent
        $scope.contact = {};
        $scope.submitMessage = function() {
            var subscribe = $scope.contact.newsletter ? 'yes' : 'no';
            agentService.sendMessage($scope.agent.user_id, $scope.contact.name, $scope.contact.phone, $scope.contact.email, $scope.contact.message, subscribe).then(function(response) {
                $scope.contact.message = '';
                var data = {
                    boldTextTitle: '',
                    textAlert: 'Message sent',
                    mode: 'success'
                }
                var modal = $scope.open(data);
                $timeout(function () { modal.close(); }, 3000);
            });
        };
        //modal contoller
        var ModalInstanceCtrl = function ($scope, $uibModalInstance, data) {
            $scope.data = data;
            $scope.close = function (/*result*/) {
                $uibModalInstance.close($scope.data);
            };
        };

        //Open modal
        $scope.open = function (data) {
            $scope.data = data;

            var modalInstance = $uibModal.open({
                template: '<div class="modal-body" style="padding:0px">' +
                                '<div class="alert alert-{{data.mode}}" style="margin-bottom:0px">' +
                                    '<button type="button" class="close" data-ng-click="close()">' +
                                        '<span class="glyphicon glyphicon-remove-circle"></span>' +
                                    '</button>' +
                                    '<strong>{{data.boldTextTitle}}</strong> {{data.textAlert}}' +
                                '</div>' +
                            '</div>',
                controller: ModalInstanceCtrl,
                backdrop: true,
                keyboard: true,
                backdropClick: true,
                size: 'sm',
                resolve: {
                    data: function () {
                        return $scope.data;
                    }
                }
            });

            return modalInstance;
        };

        //create global variables if doesn't already exist
        try {
            userIsOnline;
        } catch(err) {
            window.userIsOnline = true;
            window.onOfflineMessage = 'Please login first.';
        }
    };

    //Makes API calls
    function agentService($http, $q) {
        function _getAgentDetails(id) {
            var d = $q.defer();
            $http.get('http://demo.thaifullhouse.com/rest/agent/details/' + id).then(function (response) {
                return d.resolve(response.data);
            });
            return d.promise;
        }
        function _sendMessage(id, name, phone, email, message, subscribe) {
            var d = $q.defer();
            var data = {
                id: id,
                name: name,
                phone: phone,
                email: email,
                message: message,
                subscribe: subscribe
            }
            $http.post('http://demo.thaifullhouse.com/rest/message/send', data).then(function (response) {
                return d.resolve(response.data);
            });
            return d.promise;
        }
        function _sendReview(id, rating, review) {
            var d = $q.defer();
            var data = {
                id: id,
                rating: rating,
                review: review,
				user: currentUserId
            }
            $http.post('http://demo.thaifullhouse.com/rest/agent/review', data).then(function (response) {
                return d.resolve(response.data);
            });
            return d.promise;
        }
        return {
            getAgentDetails: _getAgentDetails,
            sendMessage: _sendMessage,
            sendReview: _sendReview
        };
    }

    //For giving custom background image to a div dynamically
    function backImg() {
        return function (scope, element, attrs) {
            attrs.$observe('backImg', function (value) {
                element.css({
                    'background-image': 'linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0)), url(' + value + ')',
                    'background-position': 'center',
                    'background-size': 'cover'
                });
            });
        };
    }

    //For giving custom background image to a div dynamically with tinted view
    function backImgBlur() {
        return function (scope, element, attrs) {
            attrs.$observe('backImgBlur', function (value) {
                element.css({
                    'background-image': 'linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), url(' + value + ')',
                    'background-position': 'center',
                    'background-size': 'cover'
                });
            });
        };
    }
})();