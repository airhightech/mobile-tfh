var map;
var f_lt = '';
var f_bed = 0;
var f_np = '';
var f_bat = 0;

var baseUrl = 'http://demo.thaifullhouse.com';

var currentId = 0;

$(function () {    

    var lat0 = lat0 || 13.738244;
    var lng0 = lng0 || 100.534074;

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            lat0 = position.coords.latitude;
            lng0 = position.coords.longitude;
        });
    }
    
    /*if (lat0 === null || lng0 === null) {
        
    }*/

    map = new google.maps.Map(document.getElementById('mapview'), {
        center: {lat: lat0, lng: lng0},
        zoom: 15
    });

    map.addListener('bounds_changed', mapBoundsListener = function () {
        reloadOnIdle = true;
        
    });

    map.addListener('idle', mapIdleListener = function () {
        if (reloadOnIdle) {
            currentPage = 1;
            //applySearchCriteria();
        }
        applySearchCriteria();
    });
    
    
    $('.apply-search-option').on('click', function(){
        $(".options-menu").hide();
        $(".options-menu").addClass('selected');
        applySearchCriteria();
    });
    
    initAutoComplete();

});

var initAutoComplete = function () {
    $('#q').autocomplete({
        source: baseUrl + "/rest/search-autocomplete",
        minLength: 2,
        classes: {
            "ui-autocomplete": "map-search"
        },
        select: selectSuggested,
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        //console.log(item);
        return $('<li class="poi-item">').addClass('prop').addClass(item.type).data('lat', item.lat).data('lng', item.lng).data('zl', item.zoom_level)
                .attr("data-value", item.id)
                .append('<span class="icon ' + item.type + '"></span><span class="name">' + item.value + '</span><span class="poi-type text-muted">' + item.type_label + '</span>' +
                        '<span class="more">' + item.more + '</span>')
                .appendTo(ul);
    };
};

var selectSuggested = function (event, ui) {

    console.log(ui.item);

    currentId = ui.item.id;

    domain = ui.item.domain;

    currentPoiData = ui.item;

    var lat = parseFloat(ui.item.lat);
    var lng = parseFloat(ui.item.lng);
    var zl = parseFloat(ui.item.zoom_level);
    map.panTo({lat: lat, lng: lng});
    //map.setZoom(zl);

    if (ui.item.domain === 'poi') {
        var poimg = getImageUrl(ui.item.type);

        if (poimg) {
            var image = {
                url: poimg,
                size: new google.maps.Size(32, 37),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 37)
            };

            var marker = new google.maps.Marker({
                position: {lat: lat, lng: lng},
                draggable: true,
                title: ui.item.item,
                icon: image,
                map: map
            });
        }

    } else {
        // Display 
        //openMakerInfoById(ui.item.id);

//        if (ui.item.domain === 'condo') {
//            $('#pt-cd').prop('checked', true);
//        }

        applySearchCriteria();
    }
};

var generateSearchUrl = function () {

    var url = '?q=' + $('#q').val();

    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    url = url + '&nl=' + sw.lat().toFixed(6) + ',' + sw.lng().toFixed(6) + '&fr=' + ne.lat().toFixed(6) + ',' + ne.lng().toFixed(6);

    url = url + '&lt=' + $('#lt').val();
    url = url + '&pt=' + $('#pt').val();

    url = url + '&pmin=' + $('#smin').val() + '&pmax=' + $('#pmax').val();

    url = url + '&bed=' + $('#bed').val();
    url = url + '&bath=' + $('#bath').val();

    url = url + '&smin=' + $('#smin').val();
    url = url + '&smax=' + $('#smax').val();

    url = url + '&ymin=' + $('#ymin').val();
    url = url + '&ymax=' + $('#ymax').val();

//    if (currentPage > 1) {
//        url = url + '&page=' + currentPage;
//    }
//
    if (currentId) {
        url = url + '&id=' + currentId;
    }
//
//    if (domain) {
//        url = url + '&rel=' + domain;
//    }

    return url;

};

var currentUrl;

var applySearchCriteria = function () {
    updateMapResult();
    updateListResult();
};


var updateMapResult = function () {

    currentUrl = generateSearchUrl();

    $.get(baseUrl + '/rest/search/pro/map' + currentUrl, function (searchResult) {
        loadPropertiesOnMap(searchResult/*data.properties*/);
    });
};

var updateListResult = function () {

    currentUrl = generateSearchUrl();

    //$('.loading-results').show();

    //window.history.pushState('page2', $('title').text(), '/search' + currentUrl);

    $.get(baseUrl + '/rest/search/pro/list' + currentUrl, function (searchResult) {

        $('.loading-results').hide();

        reloadOnIdle = false;

        $('.property-count').html(searchResult.properties.total + ' properties');

        if (searchResult.properties.last_page > 1) {
            $('.search-results-pagination').pagination({
                items: searchResult.properties.total,
                itemsOnPage: searchResult.properties.per_page,
                currentPage: searchResult.properties.current_page,
                hrefTextPrefix: '#',
                displayedPages: 3,
                onPageClick: function (pageNum, event) {
                    currentPage = pageNum;
                    updateListResult();
                },
                selectOnClick: false
            });
        } else {
            $('.search-results-pagination').html('');
        }

        loadPropertiesOnList(searchResult/*searchResult.properties.data, searchResult.favorites*/);

    });

};


var propertyMarkers = [];
var markerCluster = null;
//var markerInfos = [];
var currentInfo = null;
var currentInfoXhr;
var currentMarker = null;

var loadPropertiesOnMap = function (searchResult) {
    
    if (propertyMarkers.length) {

        for (var i = 0; i < propertyMarkers.length; i++) {
            var marker = propertyMarkers[i];
            marker.setMap(null);
        }
        propertyMarkers = [];
        markerInfos = [];
    }

    if (markerCluster) {
        markerCluster.clearMarkers();
    }

    currentMarker = null;

    if (searchResult.properties.length > 0) {
        for (var i = 0; i < searchResult.properties.length; i++) {
            var pdata = searchResult.properties[i];

            var markerIcon;

            if (pdata.np === true) {
                if (pdata.lclass === 'featured') {
                    markerIcon = '/img/maps/pin/pin-sale-featured.png';
                } else if (pdata.lclass === 'exclusive') {
                    markerIcon = '/img/maps/pin/pin-sale-exclusive.png';
                } else {
                    markerIcon = '/img/maps/pin/pin-sale-standard.png';
                }
            } else {
                if (pdata.lclass === 'featured') {
                    if (pdata.ltype === 'rent') {
                        markerIcon = '/img/maps/pin/pin-rent-featured.png';
                    } else {
                        markerIcon = '/img/maps/pin/pin-sale-featured.png';
                    }
                } else if (pdata.lclass === 'exclusive') {
                    if (pdata.ltype === 'rent') {
                        markerIcon = '/img/maps/pin/pin-rent-exclusive.png';
                    } else {
                        markerIcon = '/img/maps/pin/pin-sale-exclusive.png';
                    }
                } else {
                    if (pdata.ltype === 'rent') {
                        markerIcon = '/img/maps/pin/pin-rent-standard.png';
                    } else {
                        markerIcon = '/img/maps/pin/pin-sale-standard.png';
                    }
                }

            }



            var marker = new google.maps.Marker({
                position: {lat: parseFloat(pdata.lat), lng: parseFloat(pdata.lng)}, icon: {
                    url: markerIcon,
                    size: new google.maps.Size(40, 40),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(20, 0)
                }
            });

            marker.data = pdata;
            marker.data.type = searchResult.type;

            //var infowindow = 

            marker.infoIndex = propertyMarkers.length;

            marker.addListener('mouseover', function () {
                openMakerInfo(this);
            });

            marker.addListener('click', function () {                
                openMakerInfo(this);
            });



//            if (domain !== 'poi' && currentId > 0 && pdata.id === currentId) {
//                console.log('matching currentMarker');
//                currentMarker = marker;
//            }

            propertyMarkers.push(marker);
            //markerInfos.push(infowindow);
        }

        //console.log('currentId', currentId);

        if (markerCluster) {
            markerCluster.addMarkers(propertyMarkers);
        } else {
            var clusterStyles = [
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                }
            ];

            markerCluster = new MarkerClusterer(map, propertyMarkers,
                    {
                        imagePath: '/img/m',
                        styles: clusterStyles
                    });
        }
    }

    if (currentMarker !== null) {
        console.log('currentMarker', currentMarker);
        openMakerInfo(currentMarker);
    }

//    if (currentPoiData !== null) {
//
//        console.log('currentPoiData', currentPoiData);
//
//        if (currentPoiData.domain === 'poi') {
//            var poimg = getImageUrl(currentPoiData.type);
//
//            if (poimg) {
//                var image = {
//                    url: poimg,
//                    size: new google.maps.Size(32, 37),
//                    origin: new google.maps.Point(0, 0),
//                    anchor: new google.maps.Point(16, 37)
//                };
//
//                var marker = new google.maps.Marker({
//                    position: {lat: parseFloat(currentPoiData.lat), lng: parseFloat(currentPoiData.lng)},
//                    draggable: true,
//                    title: currentPoiData.title,
//                    icon: image,
//                    map: map
//                });
//            }
//        } else {
//            openMakerInfoById(currentPoiData.id);
//        }
//    }
};

var loadPropertiesOnList = function (searchResult) {
    
    $('.search-results').html('');

    if (searchResult.properties.data.length > 0) {

        $('.no-search-results').hide();

        for (var i = 0; i < searchResult.properties.data.length; i++) {
            var pdata = searchResult.properties.data[i];
            pdata.rank = i;
            var prop = new TFHProperty(pdata, searchResult.type);
            $('.search-results').append(prop.getTemplate(searchResult.favorites));
        }

        $('.favorite-btn').on('click', function (event) {

            event.stopPropagation();

            var elt = $(this);

            var pid = elt.data('id');

            if (pid) {
                $.get('/rest/user/favorite-property/' + pid + '?t=' + searchResult.type, function (data, status, xhr) {
                    if (data.status === 'OK') {
                        if (data.result === 'ADDED') {
                            elt.find('.fa').removeClass('fa-heart-o').addClass('fa-heart').addClass('text-danger');
                        } else {
                            elt.find('.fa').removeClass('fa-heart').removeClass('text-danger').addClass('fa-heart-o');
                        }
                    } else {

                    }
                });
            }
        });

    } else {
        $('.no-search-results').show();
        $('.property-count').html('No property found');
    }
};


var openMakerInfo = function (marker) {

    console.log('openMakerInfo');

    if (currentInfo) {
        currentInfo.close();
    }
    if (currentInfoXhr) {
        currentInfoXhr.abort();
    }

    var option = {
        disableAutoPan: false
        , maxWidth: 0
        , pixelOffset: new google.maps.Size(-125, -80)
        , zIndex: null
        , closeBoxURL: ""
        , infoBoxClearance: new google.maps.Size(1, 1)
        , isHidden: false
        , pane: "floatPane"
        , enableEventPropagation: false
    };

    currentInfo = new InfoBox(option);



    //currentInfo = new google.maps.InfoWindow();

    var rand = Math.random() * (1000000) + 1000000;

    currentInfoXhr = $.get(baseUrl + '/rest/property/map-info/' + marker.data.id + '?type=' + marker.data.type + '&t=' + rand, markerInfoLoader.bind(marker));
};

var markerInfoLoader = function (data) {

    currentInfo.setContent(data);

    if (this.map !== null) {
        currentInfo.open(this.map, this);
        currentInfoXhr = null;
    } else {
        console.log('markerInfoLoader no map');
        currentInfo.setPosition({lat: parseFloat(this.data.lat), lng: parseFloat(this.data.lng)});
        currentInfo.open(map);
    }
};