

var TFHProperty = function _TFH_Property(data, type) {
    this.data = data;
    this.type = type;
};

TFHProperty.prototype.loadImage = function (data) {
    if (data.main !== null) {

    }
};

TFHProperty.prototype.addToFavorite = function () {

};

TFHProperty.prototype.getTemplate = function (favorites) {

    var html = $('#property-template').html();
    var elt = $(html);
    //elt.find('.img-count').html(this.data.images.length + ' images');

    elt.find('.prop-image').prop('src', '/property-thumb/' + this.data.id);
    elt.prop('id', 'prop-' + this.data.id);

    elt.find('.listing-type').addClass(this.data.ltype);

    var price = 0;
    
    var filterPrice = 0;

    if (this.type === 'prop') {
        
        elt.find('.beds').html(this.data.bedroom_num);
        elt.find('.bath').html(this.data.bathroom_num);
        elt.find('.size').html(this.data.total_size);
        
        filterPrice = this.data.listing_price;
        
        price = numberWithCommas(this.data.listing_price);
        price = (price === null) ? 0 : price;
        elt.find('.listing-price').html('‎฿ ' + price);
        
        if (this.data.name) {
            var name = $.parseJSON(this.data.name);
            var current = name[currentHL].length > 0 ? name[currentHL] : name['th'];
            elt.find('.property').html(current);
        } else {
            
        }  
        
    } else {
        elt.find('.prop-only').hide();
        
        filterPrice = this.data.starting_sales_price;
        
        price = numberWithCommas(this.data.starting_sales_price);
        price = (price === null) ? 0 : price;
        elt.find('.listing-price').html('‎฿ ' + price);
        
        var name = $.parseJSON(this.data.name);        
        var current = name[currentHL].length > 0 ? name[currentHL] : name['th'];
        elt.find('.property').html(current);
        
        var address = $.parseJSON(this.data.address);   
        
        if (address) {
            current = address[currentHL].length > 0 ? address[currentHL] : address['th'];
        }
        else {
            current = this.data.location_name;
        }        
        
        elt.find('.address').html(current);        
    }

    elt.data('rank', this.data.rank);
    elt.data('year', this.data.year_built);
    elt.data('price', filterPrice);
    elt.data('beds', this.data.bedroom_num);
    elt.data('baths', this.data.bathroom_num);
    elt.data('size', this.data.total_size);
    elt.data('rental', this.data.minimal_rental_period);

    elt.on('click', this.showPreview.bind(this));

    // Set favorite icon

    elt.find('.favorite-btn').data('id', this.data.id);

    if (favorites.length > 0 && favorites.indexOf(this.data.id) >= 0) {
        elt.find('.favorite').addClass('fa-heart').addClass('text-danger');
    } else {
        elt.find('.favorite').addClass('fa-heart-o');
    }

    return elt;
};

TFHProperty.prototype.showPreview = function () {
    showPropertyPreview(this.data);
};

