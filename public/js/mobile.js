$(function(){
    
    $('.favorite-container').load('/user/favorites');
    
    $('.topbar .menu').on('click', function(){
        $('.menu-container').toggleClass('visible');
    });
    
    $('.topbar .favorite').on('click', function(){
        
        $('.menu-container').removeClass('visible');
        
        if (!$('.favorite-container').hasClass('visible')) {
            $('.favorite-container').load('/user/favorites');
        }        
        $('.favorite-container').toggleClass('visible');
    });
    
});