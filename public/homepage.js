(function () {
    'use strict';

    angular
        .module('myApp', [])
        .controller('myController', myController);

    myController.$inject = ['$scope'];

    function myController($scope) {
        $scope.search = function() {
            var keyword = $('input#search').val();
            if(!(keyword.length > 0)) {
                return;
            }
            var id = $('.big-button.selected').attr('id');
            switch(id) {
                case 'rent':
                    window.location = '/search?q=' + keyword + '&lt=rent';
                    break;
                case 'buy':
                    window.location = '/search?q=' + keyword + '&lt=sale';
                    break;
                case 'condo':
                    window.location = '/search?q=' + keyword + '&pt=cdb';
                    break;
                case 'new':
                    window.location = '/search?q=' + keyword + '&np=yes';
                    break;
            }
        };
        angular.element(document).ready(function () {
            var h = $('.big-button').height();
            $('.big-button').css({ 'width': h + 4 });
            var tw = $('#inner-main').width();
            $('.spacers').css({ 'width': (tw - (2 * (h + 4))) });
        });

        $scope.selectButton = function(buttonId) {
            $('.big-button').removeClass('selected');
            $('#' + buttonId).addClass('selected');
        };
        $scope.selectedTab = 0; //default 0
        $scope.selectTab = function(tab) {
            switch(tab) {
                case 0:
                    $scope.selectedTab = 1;
                    $('.home-tab').removeClass('selected');
                    $('#tab0').addClass('selected');
                    break;
                case 1:
                    $scope.selectedTab = 2;
                    $('.home-tab').removeClass('selected');
                    $('#tab1').addClass('selected');
                    break;
                case 2:
                    $scope.selectedTab = 3;
                    $('.home-tab').removeClass('selected');
                    $('#tab2').addClass('selected');
                    break;
            }
        };

        
    }
})();